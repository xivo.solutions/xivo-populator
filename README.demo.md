# XiVO Populator Demo Setup Guide

This guide will walk you through setting up a XiVO demo environment using the XiVO Populator tool.

## Prerequisites

1. A machine with XiVO PBX, CC, and Meeting Room installed
2. A separate "load" machine for running the populator and tests
3. Docker and Docker Compose installed on the load machine
4. Git installed on the load machine

## Setup Steps
### 1. Clone the Populator Project

On your load machine, run:

```bash
git clone --recurse-submodules git@gitlab.com:xivo.solutions/xivo-populator.git && cd xivo-populator && project_path=$(pwd)
```

### 2. Configure the Populator
Create and edit the configuration file:
```bash
nano $project_path/populator/code/config/config.yml
```
Use the [config.yml](#configyml-for-populator) template provided at the end of this guide, making sure to update the credentials section with your XiVO system's details.

### 3. Configure the Gatling
Create and edit the configuration file:
```bash
nano $project_path/gatling/code/config/config.yml
```
Use the [config.yml](#configyml-for-gatling) template provided at the end of this guide.

### 4. Run the Populator
Execute the following command:
```bash
cd $project_path && docker compose -f docker-compose-populator.yml up --build
```

### 5. Manual Configuration
After the populator finishes, manually configure the following in the XiVO web interface:

- For the queue named "Acceuil":
  - Add "xivo_subr_switchboard" in the preprocess_subroutine field
- For the queue "Le commerce":
  - Select "Recorded on demand" in the recording options
- For the queue "Support":
  - Select "Recorded" in the recording options


### 6. Export User Data

1. Export the user list as a CSV file from the XiVO web interface
2. Copy the exported CSV to two locations:
    ```bash
    cp path/to/export_user.csv $project_path/xivo-load-tester/etc/demo/user_export.csv
    cp path/to/export_user.csv $project_path/gatling/code/config/user_export.csv
    ```

### 7. Install ttmux
Install and configure ttmux for simulating activites:
```bash
git clone https://github.com/sensorario/ttmux.git /opt/ttmux
ln -s /opt/ttmux/ttmux /usr/local/bin/ttmux
sed -i.bak "1 s/.*/\#\!\/usr\/bin\/bash/" /opt/ttmux/ttmux
sed -i 's/start=1/start=0/g' /opt/ttmux/ttmux
```

### 8. Run Load Tests and gatling
Launch the load tests using ttmux:
```bash
cd $project_path/xivo-load-tester/ && ttmux demo.ttmux
```
This command will start both the load tester and Gatling with the appropriate scenarios.

## Additional Information

The provided [config.yml](#configyml) sets up various contexts, users, and queues in your XiVO system.
The load tests simulate SIP calls and agent activities in queues.
Make sure your XiVO system can handle the load before running tests in a production environment.

## config.yml for gatling
```yaml
xivocc_ip: '10.35.2.101'
xuc_port: 8090
run_time: 0
users_file:
  csv_path: config/user_export.csv
  filter:
    labels:
      - Surcharge
      - Moyen
      - Tranquille
    number_range:
      - '1000'
      - '1999'
```

## config.yml for populator
```yaml
# Configuration settings for xivo-populator
# Choose 'Create' to set up entities, 'Delete' to remove specific entities
# WARNING, mode 'DeleteEveything' will delete all XiVO object even if their not specify in this yml !
status: Create

# Special mode setting: use 'dry' for testing without making actual changes in XiVO
specials: abcd
 
# Credentials for accessing the XiVO system
credentials:
  xivo_ip: "10.35.2.100"            # IP address or FQDN of the XiVO PBX
  ws_user: api-user                 # Username for API access (webservice)
  ws_pass: "Superpass12345!"        # Password for API access (webservice)
  webi_login: root                  # Web interface login username (webi)
  webi_pwd: "WEBI_PASSWORD"         # Web interface login password (webi)

contexts:
  from-externe-demo:
    name: from-externe-demo
    display_name: 'Appel entrants demo'
    context_type: 'incall'
    include_sub_contexts: null
    user:
      first_number: null
      last_number:  null
    group:
      first_number: null
      last_number:  null
    queue:
      first_number: null
      last_number:  null
    meetme:
      first_number: null
      last_number:  null
    incall:
      first_number: "0000000000"
      last_number:  "9999999999"
      did_length: 10

  to-externe-demo:
    name: to-externe-demo
    display_name: 'Appel sortant demo'
    context_type: 'outcall'
    include_sub_contexts: null
    user:
      first_number: null
      last_number:  null
    group:
      first_number: null
      last_number:  null
    queue:
      first_number: null
      last_number:  null
    meetme:
      first_number: null
      last_number:  null
    incall:
      first_number: null
      last_number:  null
      did_length: null

  default-demo:                    
    name: default-demo              
    display_name: 'default demo'    
    context_type: 'internal'        
    include_sub_contexts:         
      - from-externe-demo
    user:
      first_number: '0000'
      last_number:  '1999'
    group:
      first_number: '2000'
      last_number:  '2999'
    queue:
      first_number: '3000'
      last_number:  '3999'
    meetme:
      first_number: '4000'
      last_number:  '4999'
    incall:
      first_number: null
      last_number:  null
      did_length: null

  loadtest:
    name: loadtest
    display_name: 'loadtest'
    context_type: 'incall'
    include_sub_contexts:
    - 'default-demo'
    user:
      first_number: null
      last_number:  null
    group:
      first_number: null
      last_number:  null
    queue:
      first_number: null
      last_number:  null
    meetme:
      first_number: null
      last_number:  null
    incall:
      first_number: null
      last_number:  null
      did_length: null

users:
  myfirstuserrange:
    context: default-demo
    user_prefix: GenerateFakeName
    line_type: sip
    first_number: '0100'
    last_number: '0149'
    mds: default
    label: 'interne'

queues:
  Commerce:
    name: commerce
    display_name: "Le commerce"
    number: '3000'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1000'
      last_number: '1001'
      mds: default
      label: 'Commerce'
    recording: "Recorded on demand"

  Support:
    name: Support
    display_name: "Le support"
    number: '3001'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1010'
      last_number: '1011'
      mds: default
      label: 'Support'
    recording: "Recorded"

  Acceuil:
    name: Acceuil
    display_name: "accueil"
    number: '3002'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1020'
      last_number: '1021'
      mds: default
      label: 'Acceuil'
    preprocess_subroutine: "xivo_subr_switchboard"

  Accueil_Hold:
    name: Accueil_Hold
    display_name: "Accueil_Hold"
    number: '3003'
    context: default-demo

  Rappel:
    name: Rappel
    display_name: "Rappel"
    number: '3004'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1040'
      last_number: '1041'
      mds: default
      label: 'Rappel'

  Tranquille:
    name: Tranquille
    display_name: "File peu chargée"
    number: '3005'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1050'
      last_number: '1059'
      mds: default
      label: 'Tranquille'

  Moyen:
    name: Moyen
    display_name: "File moyennement chargée"
    number: '3006'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1060'
      last_number: '1069'
      mds: default
      label: 'Moyen'

  Surcharge:
    name: Surcharge
    display_name: "File très chargée"
    number: '3007'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1070'
      last_number: '1079'
      mds: default
      label: 'Surcharge'

  RappelCampagne:
    name: RappelCampagne
    display_name: "campagne de rappel"
    number: '3021'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1080'
      last_number: '1081'
      mds: default
      label: 'RappelCampagne'

  RappelSVI:
    name: RappelSVI
    display_name: "rappel choix SVI"
    number: '3020'
    context: default-demo
    agents:
      context: default-demo
      user_prefix: GenerateFakeName
      line_type: sip
      first_number: '1090'
      last_number: '1091'
      mds: default
      label: 'RappelSVI'

trunks:
 internaltests:
    name: internaltests
    authentification_username: 'internaltests'
    password: internaltests
    caller_id: internaltests
    call_limit: "0"                    # Call limit for the trunk, Possible values : From 0 to 50 (0 being unlimited)
    connection_type: yser              # The connection type (user, peer or friend)
    ip_addressing_type: dynamic        # The addressing type (static or dynamic) (If dynamic, no need to fill ipAddress field)
    context: "loadtest"                # Context in which the trunk is created
    mds_id: "1"                        # ID of the concerned mds (Value 1 for the MDS-main)
    nat: "no"                          # The NAT configuration (null, no, force_rport, comedia or force_rport or comedia)

```

