playwright==1.41.0
requests==2.31.0
PyYAML==6.0.1
pyee==11.0.1
faker==9.0.1