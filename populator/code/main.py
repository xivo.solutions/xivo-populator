from typing import Dict, Any
from utils import setup_logger
from populate import Populate

logger = setup_logger()

def main() -> None:
    logger.info("Starting XiVO-Populator solution...")
    populate: Populate = Populate()
    status: str = populate.yaml_loader.load("config.yml", ["status"])
    
    if status == "DeleteEverything":
        logger.info("Detected status: DeleteEverything")
        logger.info("Deleting all objects in XiVO...")
        populate.xivo_client.update_live_reload_status(False)

        populate.delete_everything()

        populate.xivo_client.update_live_reload_status(True)

        logger.info("Finished deleting all objects in XiVO")

    elif status == "Delete":
        logger.info("Detected status: Delete")
        logger.info("Deleting all objects specified in config.yml...")
        populate.xivo_client.update_live_reload_status(False)

        populate.delete_incoming_calls()
        populate.delete_trunks()
        populate.delete_queues()
        populate.delete_users()
        populate.delete_contexts()

        populate.xivo_client.update_live_reload_status(True)

        logger.info("Finished all objects specified in config.yml...")

    elif status == "Create":
        logger.info("Detected status: Create")
        populate.xivo_client.update_live_reload_status(False)

        populate.add_contexts()
        populate.add_simple_users()
        populate.add_queues()
        populate.add_trunks()
        populate.add_incoming_calls()

        populate.xivo_client.update_live_reload_status(True)

        logger.info("Finished populating XiVO")
        
    else:
        logger.error(
            f"status in config.yml is not valid, it should be 'Create', 'Delete' or 'DeleteEverything', not {status}"
        )
        exit(1)

    logger.info("Finished XiVO-Populator solution")

if __name__ == "__main__":
    main()
