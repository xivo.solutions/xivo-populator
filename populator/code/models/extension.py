class Extension:
    """
    Represents an extension in XiVO with an ID, context, commented, and extension.
    """

    def __init__(
        self,
        id: int,
        context: str,
        exten: str,
        commented: bool,
    ):
        self.id = id
        self.context = context
        self.commented = commented
        self.exten = exten
