class Line:
    """
    Represents a line in XiVO with various configuration attributes.

    Attributes:
        id (int): The ID of the line.
        context (str): The context of the line.
        caller_id_name (str): The caller ID name of the line.
        caller_id_num (str): The caller ID number of the line.
        device_id (str): The ID of the device associated with the line.
        device_slot (int): The slot number of the device associated with the line.
        name (str): The name of the line.
        protocol (str, optional): The protocol used by the line (default is "sip").
        provisioning_extension (str, optional): The provisioning extension of the line (default is "").
        provisioning_code (str, optional): The provisioning code of the line (default is "").
        position (int, optional): The position of the line (default is 0).
        registrar (str, optional): The registrar of the line (default is "string").
    """

    def __init__(
        self,
        id: int,
        context: str,
        caller_id_name: str,
        caller_id_num: str,
        device_id: str,
        device_slot: int,
        name: str,
        protocol: str = "sip",
        provisioning_extension: str = "",
        provisioning_code: str = "",
        position: int = 0,
        registrar: str = "string",
    ):
        self.id = id
        self.context = context
        self.caller_id_name = caller_id_name
        self.caller_id_num = caller_id_num
        self.device_id = device_id
        self.device_slot = device_slot
        self.name = name
        self.protocol = protocol
        self.provisioning_extension = provisioning_extension
        self.provisioning_code = provisioning_code
        self.position = position
        self.registrar = registrar

    def __str__(self):
        return (
            f"Line ID: {self.id}\n"
            f"Context: {self.context}\n"
            f"Caller ID Name: {self.caller_id_name}\n"
            f"Caller ID Number: {self.caller_id_num}\n"
            f"Device ID: {self.device_id}\n"
            f"Device Slot: {self.device_slot}\n"
            f"Name: {self.name}\n"
            f"Protocol: {self.protocol}\n"
            f"Provisioning Extension: {self.provisioning_extension}\n"
            f"Provisioning Code: {self.provisioning_code}\n"
            f"Position: {self.position}\n"
            f"Registrar: {self.registrar}\n"
        )
