from typing import Optional
class Label:
    """
    Represents a label in XiVO with an ID, display name, and optional description.
    """

    def __init__(
        self,
        id: int,
        display_name: str,
        description: Optional[str] = None,
    ):
        self.id = id
        self.display_name = display_name
        self.description = description

    def __str__(self):
        return (
            f"Label ID: {self.id}\n"
            f"Display name: {self.display_name}\n"
            f"Description: {self.description}\n"
        )
