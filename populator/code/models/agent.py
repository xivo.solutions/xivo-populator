class Agent:
    """
    Represents an agent in XiVO.

    Attributes:
        id (int): The ID of the agent.
        firstname (str): The first name of the agent.
        lastname (str): The last name of the agent.
        number (str): The phone number of the agent.
        numgroup (int): The group number of the agent.
        context (str): The context of the agent.
        language (str): The language of the agent.
        description (str, optional): The description of the agent. Defaults to "".
        passwd (str, optional): The password of the agent. Defaults to "".
    """

    def __init__(
        self,
        id: int,
        firstname: str,
        lastname: str,
        number: str,
        numgroup: int,
        context: str,
        language: str,
        description: str = "",
        passwd: str = "",
    ):
        self.id = id
        self.firstname = firstname
        self.lastname = lastname
        self.number = number
        self.numgroup = numgroup
        self.context = context
        self.language = language
        self.description = description
        self.passwd = passwd

    def __str__(self):
        return (
            f"Agent ID: {self.id}\n"
            f"First Name: {self.firstname}\n"
            f"Last Name: {self.lastname}\n"
            f"Number: {self.number}\n"
            f"Group Number: {self.numgroup}\n"
            f"Context: {self.context}\n"
            f"Language: {self.language}\n"
            f"Description: {self.description}\n"
            f"Password: {self.passwd}\n"
        )
