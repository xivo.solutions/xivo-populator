from typing import Dict, List, Optional
class Device:
    """
    Represents a device entity in XiVO.

    Attributes:
        id (str): Device ID.
        ip (Optional[str]): IP address (10.0.0.0).
        mac (str): MAC address (aa:bb:cc:dd:ee:ff).
        sn (Optional[str]): Serial number.
        plugin (str): Provisioning plugin used by the device.
        vendor (str): Vendor name.
        model (str): Device model.
        version (Optional[str]): Firmware version.
        description (Optional[str]): Description of the device.
        status (str): Device status.
        template_id (Optional[str]): ID of the device template.
        options (Optional[str]): List of standard keys.
        links (List[Dict[str, str]]): List of device links.
    """

    def __init__(
        self,
        id: str,
        ip: Optional[str],
        mac: str,
        sn: Optional[str],
        plugin: str,
        vendor: str,
        model: str,
        version: Optional[str],
        description: Optional[str],
        status: str,
        template_id: Optional[str],
        options: Optional[str],
        links: List[Dict[str, str]],
    ):
        self.id = id
        self.ip = ip
        self.mac = mac
        self.sn = sn
        self.plugin = plugin
        self.vendor = vendor
        self.model = model
        self.version = version
        self.description = description
        self.status = status
        self.template_id = template_id
        self.options = options
        self.links = links

    def __str__(self):
        return (
            f"Device ID: {self.id}\n"
            f"IP Address: {self.ip}\n"
            f"MAC Address: {self.mac}\n"
            f"Serial Number: {self.sn}\n"
            f"Plugin: {self.plugin}\n"
            f"Vendor: {self.vendor}\n"
            f"Model: {self.model}\n"
            f"Firmware Version: {self.version}\n"
            f"Description: {self.description}\n"
            f"Status: {self.status}\n"
            f"Template ID: {self.template_id}\n"
            f"Options: {self.options}\n"
            f"Links: {self.links}\n"
        )
