from typing import Dict, List, Optional
class User:
    """
    Represents a user in XiVO with various attributes related to personal information,
    account settings, and user preferences.

    Attributes:
        id (int): Unique identifier for the user.
        uuid (str): Universally unique identifier of the user.
        username (str): Username for the user, primarily used for CTI account.
        password (str): Password for the user's CTI account.
        email (Optional[str]): Email address of the user.
        firstname (Optional[str]): First name of the user. Mandatory during creation.
        lastname (Optional[str]): Last name of the user.
        timezone (Optional[str]): Time zone preference of the user.
        language (Optional[str]): Language preference of the user.
        description (Optional[str]): A brief description or notes about the user.
        caller_id (str): Caller ID for the user.
        outgoing_caller_id (Optional[str]): Outgoing caller ID for the user.
        mobile_phone_number (Optional[str]): Mobile phone number of the user.
        music_on_hold (Optional[str]): Music on hold preference for the user.
        preprocess_subroutine (Optional[str]): Preprocess subroutine setting for the user.
        userfield (Optional[str]): Additional user-defined field for the user.
        call_transfer_enabled (bool): Flag indicating if call transfer is enabled.
        call_record_enabled (bool): Flag indicating if call recording is enabled.
        online_call_record_enabled (bool): Flag indicating if online call recording is enabled.
        supervision_enabled (bool): Flag indicating if supervision is enabled for the user.
        ring_seconds (Optional[int]): The number of seconds the user's phone rings before a call is diverted.
        simultaneous_calls (Optional[int]): Number of simultaneous calls allowed for the user.
        call_permission_password (Optional[str]): Password for call permission.
        agent_number (Optional[str]): Agent number assigned to the user.
        agent_group_id (Optional[int]): Group ID of the agent group to which the user belongs.
        agentid (Optional[str]): Unique identifier for the agent.
        enabled (bool): Flag indicating if the user account is enabled.
        links (List[Dict[str, str]]): Hyperlinks related to the user.
        labels (List[str]): Labels or tags associated with the user.
    """


    def __init__(
        self,
        id: int,
        uuid: str,
        username: str,  # for cti acuser_count
        password: str,  # for cti acuser_count
        email: Optional[str] = None,
        firstname: Optional[
            str
        ] = None,  # the only field that is mandatory at the creation
        lastname: Optional[str] = None,
        timezone: Optional[str] = None,
        language: Optional[str] = None,
        description: Optional[str] = None,
        caller_id: str = "",
        outgoing_caller_id: Optional[str] = None,
        mobile_phone_number: Optional[str] = None,
        music_on_hold: Optional[str] = None,
        preprocess_subroutine: Optional[str] = None,
        userfield: Optional[str] = None,
        call_transfer_enabled: bool = False,
        call_record_enabled: bool = False,
        online_call_record_enabled: bool = False,
        supervision_enabled: bool = False,
        ring_seconds: Optional[int] = 20,
        simultaneous_calls: Optional[int] = 2,
        call_permission_password: Optional[str] = None,
        agent_number: Optional[str] = None,
        agent_group_id: Optional[int] = None,
        agentid: Optional[str] = None,
        enabled: bool = False,
        links: List[Dict[str, str]] = [],
        labels: List[str] = [],
    ):
        self.id = id
        self.uuid = uuid
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.timezone = timezone
        self.language = language
        self.description = description
        self.caller_id = caller_id
        self.outgoing_caller_id = outgoing_caller_id
        self.mobile_phone_number = mobile_phone_number
        self.username = username
        self.password = password
        self.music_on_hold = music_on_hold
        self.preprocess_subroutine = preprocess_subroutine
        self.userfield = userfield
        self.call_transfer_enabled = call_transfer_enabled
        self.call_record_enabled = call_record_enabled
        self.online_call_record_enabled = online_call_record_enabled
        self.supervision_enabled = supervision_enabled
        self.ring_seconds = ring_seconds
        self.simultaneous_calls = simultaneous_calls
        self.call_permission_password = call_permission_password
        self.agent_number = agent_number
        self.agent_group_id = agent_group_id
        self.agentid = agentid
        self.enabled = enabled
        self.links = links
        self.labels = labels


    def __str__(self):
        return (
            f"User ID: {self.id}\n"
            f"UUID: {self.uuid}\n"
            f"First Name: {self.firstname}\n"
            f"Last Name: {self.lastname}\n"
            f"Email: {self.email}\n"
        )

    def full_print(self):
        return (
            f"User ID: {self.id}\n"
            f"UUID: {self.uuid}\n"
            f"First Name: {self.firstname}\n"
            f"Last Name: {self.lastname}\n"
            f"Email: {self.email}\n"
            f"Timezone: {self.timezone}\n"
            f"Language: {self.language}\n"
            f"Description: {self.description}\n"
            f"Caller ID: {self.caller_id}\n"
            f"Outgoing Caller ID: {self.outgoing_caller_id}\n"
            f"Mobile Phone Number: {self.mobile_phone_number}\n"
            f"Username: {self.username}\n"
            f"Password: {self.password}\n"
            f"Music on Hold: {self.music_on_hold}\n"
            f"Preprocess Subroutine: {self.preprocess_subroutine}\n"
            f"Userfield: {self.userfield}\n"
            f"Call Transfer Enabled: {self.call_transfer_enabled}\n"
            f"Call Record Enabled: {self.call_record_enabled}\n"
            f"Online Call Record Enabled: {self.online_call_record_enabled}\n"
            f"Supervision Enabled: {self.supervision_enabled}\n"
            f"Ring Seconds: {self.ring_seconds}\n"
            f"Simultaneous Calls: {self.simultaneous_calls}\n"
            f"Call Permission Password: {self.call_permission_password}\n"
            f"Agent Number: {self.agent_number}\n"
            f"Agent Group ID: {self.agent_group_id}\n"
            f"Agent ID: {self.agentid}\n"
            f"Enabled: {self.enabled}\n"
            f"Links: {self.links}\n"
            f"Labels: {self.labels}\n"
        )
