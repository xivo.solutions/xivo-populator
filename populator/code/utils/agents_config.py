# /usr/bin/python

import json

from utils.xivo_api_client import XiVOAPIClient
from utils.xivo_api_client import Agent, User
from utils.logger import setup_logger
from typing import List
from faker import Faker

fake = Faker()
logger = setup_logger()


def create_agents(
    client: XiVOAPIClient,
    queue_id: str,
    user_prefix: str,
    first_exten: str,
    last_exten: str,
    context: str,
):
    logger.debug("Creating agents..")
    GenerateFakeName = False

    if user_prefix == "GenerateFakeName":
        GenerateFakeName = True   
        users_list: List[User] = client.list_users()


    index = 1
    for extension in range(int(first_exten), int(last_exten) + 1):
        formated_exten = str(extension).zfill(len(str(first_exten)))

        # Find the user with the specified extension
        if GenerateFakeName:
            user = None
            for u in users_list:
                if u.mobile_phone_number == formated_exten:
                    user = u
                    break
            
            if user:
                data = {
                    "firstname": user.firstname,
                    "lastname": user.lastname,
                    "number": f"{formated_exten}",
                    "numgroup": "1",
                    "context": f"{context}",
                }
            else:
                logger.warning(f"User with extension {formated_exten} not found. Skipping agent creation.")
                continue
        else:
            data = {
                "firstname": f"{user_prefix}-{str(index)}",
                "lastname": f"{extension}",
                "number": f"{formated_exten}",
                "numgroup": "1",
                "context": f"{context}",
            }
        client.create_agent(data)
        index += 1

    count = int(last_exten) - int(first_exten) + 1

    first_user_uuid = get_first_user_uuid(client, first_exten)
    first_user_id = get_first_user_id(client, first_user_uuid, first_exten)
    first_agent_id = get_first_agent_id(client, first_exten)
    update_users_with_agents(client, first_user_id, first_agent_id, count)
    for agent_id in range(first_agent_id, first_agent_id + count):
        client.populate_queue_agent(int(queue_id), agent_id)


def get_first_user_uuid(client: XiVOAPIClient, first_exten):
    users_list = json.loads(client.export_csv_json().text)
    matched_uuids = [
        item["uuid"]
        for item in users_list["content"]
        if item.get("exten") == first_exten
    ]
    if matched_uuids:
        first_user_uuid = matched_uuids[0]
        return first_user_uuid
    else:
        print(f"No user with 'number' {first_exten} found.")
        exit()


def get_first_user_id(client: XiVOAPIClient, first_user_uuid, first_exten):
    users_list: List[User] = client.list_users()
    matched_ids = [client.id for client in users_list if client.uuid == first_user_uuid]
    if matched_ids:
        first_user_id = matched_ids[0]
        return first_user_id
    else:
        print(f"No user with 'number' {first_exten} found.")
        exit()


def get_first_agent_id(client: XiVOAPIClient, first_exten):
    agents_list: List[Agent] = client.list_agents()
    matched_ids = [agent.id for agent in agents_list if agent.number == first_exten]
    if matched_ids:
        first_agent_id = matched_ids[0]
        return first_agent_id
    else:
        logger.error(f"No agent with 'number' {first_exten} found.")
        exit()


def update_users_with_agents(
    client: XiVOAPIClient, first_user_id, first_agent_id, count
):
    current_agent_id = first_agent_id
    for user_id in range(first_user_id, first_user_id + count):
        data = {"id": user_id, "agentid": current_agent_id}
        current_agent_id += 1
        client.update_user(user_id, data)
