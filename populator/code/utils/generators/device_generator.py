#!/usr/bin/python3

import re
import random
import sys

from utils.logger import setup_logger

logger = setup_logger()

def create_devices(first_exten: str, amount: int, plugin_config={}):
    mac_base = 500000000000
    status = "autoprov"
    entity_name = "showroom"
    plugin_config = {
        "Yealink": {
            "xivo-yealink-v85": ["T31P", "T33G", "T53", "T54W", "T57W", "W73P"]
        },
        "Snom": {
            "xivo-snom-10.1.46.16": [
                "D712",
                "715",
                "D715",
                "D717",
                "D725",
                "D735",
                "D765",
                "D785",
            ]
        },
    }
    phones = []
    for exten in range(amount):
        phone = _pick_random_phone_plugin(plugin_config)
        mac = _format_mac_address(mac_base + int(first_exten) + exten)
        phones.append(
            {
                "mac": mac,
                "status": status,
                "vendor": phone["vendor"],
                "model": phone["model"],
                "plugin": phone["plugin"],
                "entity": entity_name,
            }
        )

    return phones


def _pick_random_phone_plugin(plugin_config):
    vendors = list(plugin_config.keys())
    vendor = random.choice(vendors)  # eg. Yealink
    plugins = list(plugin_config[vendor].keys())
    plugin = random.choice(plugins)  # eg. xivo-yealink-v85
    model = random.choice(plugin_config[vendor][plugin])  # eg. 370

    return {"vendor": vendor, "model": model, "plugin": plugin}


def _format_mac_address(mac_number):
    unformatted_mac = str(mac_number)
    mac_formatted = ":".join(re.findall("..", unformatted_mac))
    return mac_formatted


def main():
    first_exten = int(sys.argv[1])
    amount = int(sys.argv[2])
    create_devices(first_exten, amount)


if __name__ == "__main__":
    main()
