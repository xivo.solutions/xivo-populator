#!/usr/bin/python3

import io
import csv
import sys
from faker import Faker

fake = Faker()

from utils.logger import setup_logger

logger = setup_logger()


def create_users(
    user_prefix: str,
    line_type: str,
    first_exten: str,
    password: str,
    last_exten: str,
    context: str,
    mds: str,
    label_name: str,
):
    logger.debug("Creating users..")
    GenerateFakeName = False
    
    if user_prefix == "GenerateFakeName":
        GenerateFakeName = True

    users = []
    for exten in range(int(first_exten), int(last_exten) + 1):
        formated_exten = str(exten).zfill(len(str(first_exten)))
        firstname = fake.first_name() if GenerateFakeName else user_prefix + formated_exten
        lastname = fake.last_name() if GenerateFakeName else formated_exten
        csv_user = {
            "entity_id": 1,
            "firstname": firstname,
            "lastname": lastname,
            "outgoing_caller_id": "default",
            "language": "fr_FR",
            "mobile_phone_number": formated_exten,
            "enabled": 1,
            "ring_seconds": 30,
            "simultaneous_calls": 5,
            "supervision_enabled": 1,
            "call_transfer_enabled": 1,
            "username": formated_exten,
            "password": password,
            "cti_profile_name": "Agent",
            "cti_profile_enabled": 1,
            "line_protocol": line_type,
            "line_site": mds,
            "context": context,
            "exten": formated_exten,
            "labels": label_name,
        }
        users.append(csv_user)

    output = io.StringIO()
    field_names = users[0].keys()
    writer = csv.DictWriter(output, fieldnames=field_names)
    writer.writeheader()
    writer.writerows(users)
    return output.getvalue()
