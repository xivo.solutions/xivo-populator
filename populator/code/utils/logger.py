import logging

# Define ANSI color codes
class LogColors:
    HEADER = '\033[95m'
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[33;20m'
    RED = '\033[0;31m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Logging formats with color
FORMATS = {
    logging.DEBUG: LogColors.BLUE + "%(asctime)s - XiVO-Populator - %(levelname)s - %(funcName)s - %(message)s (%(filename)s:%(lineno)d)" + LogColors.ENDC,
    logging.INFO: LogColors.CYAN + "%(asctime)s - XiVO-Populator - %(levelname)s - %(message)s" + LogColors.ENDC,
    logging.WARNING: LogColors.WARNING + "%(asctime)s - XiVO-Populator - %(levelname)s - %(funcName)s - %(message)s (%(filename)s:%(lineno)d)" + LogColors.ENDC,
    logging.ERROR: LogColors.RED + "%(asctime)s - XiVO-Populator - %(levelname)s - %(funcName)s - %(message)s (%(filename)s:%(lineno)d)" + LogColors.ENDC,
    logging.CRITICAL: LogColors.BOLD + LogColors.RED + "%(asctime)s - XiVO-Populator - %(levelname)s - %(funcName)s - %(message)s (%(filename)s:%(lineno)d)" + LogColors.ENDC,
}

class CustomFormatter(logging.Formatter):
    def format(self, record):
        log_fmt = FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

def setup_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)  # Adjust as needed

    if not logger.handlers:
        # Configure handler only if no handlers are already attached
        handler = logging.StreamHandler()
        handler.setFormatter(CustomFormatter())
        logger.addHandler(handler)

    return logger

logger = setup_logger()

# # Test logging at different levels
# logger.debug("This is a debug message.")
# logger.info("This is an info message.")
# logger.warning("This is a warning message.")
# logger.error("This is an error message.")
# logger.critical("This is a critical message.")
