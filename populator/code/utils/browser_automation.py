from typing import List, Dict, Union, Any

from playwright.sync_api import sync_playwright, TimeoutError as PlaywrightTimeoutError

import time

from utils.logger import setup_logger

logger = setup_logger()

# pylint: disable=locally-disabled, logging-fstring-interpolation
# We can use fstring in logging if the function call in fstring isn't too big (ex: print variable)


class BrowserAutomation:
    """
    A class for automating browser actions in a web application.

    Args:
        base_url (str): The base URL of the web application.
        login (str): The login username.
        pwd (str): The login password.
        ignore_ssl (bool, optional): Whether to ignore SSL errors. Defaults to False.

    Attributes:
        base_url (str): The base URL of the web application.
        login (str): The login username.
        pwd (str): The login password.
        browser: The browser instance.
        page: The current page in the browser.

    Methods:
        launch_browser: Launches the browser and returns the page instance.
        connect_to_webi: Connects to the web interface with the provided credentials.
        get_number_of_pages: Returns the number of pages based on the given selector.
        get_queue: Retrieves the list of queues from the web application.
        get_trunk: Retrieves the list of trunks from the web application.
        get_context: Retrieves the list of contexts from the web application.

    """

    def __init__(self, base_url: str, login: str, pwd: str, ignore_ssl: bool = False):
        self.base_url = base_url
        self.login = login
        self.pwd = pwd
        self.browser = None
        self.ignore_ssl = ignore_ssl

        self.page, self.context = self.launch_browser()

        self.api_request_context = self.context.request

        # As webi dosn't have a timeout, we need to connect only one time
        try:
            self.connect_to_webi(f"{self.base_url}")
        except PlaywrightTimeoutError:
            logger.error(f"Timeout while connecting to the web interface, please check that you can access the web interface with the provided URL {self.base_url}")
            raise

    def launch_browser(self):
        playwright = sync_playwright().start()
        self.browser = playwright.chromium.launch(
            headless=True,
            args=[
                "--disable-setuid-sandbox",
                "--disable-accelerated-2d-canvas",
                "--disable-dev-shm-usage",
                "--autoplay-policy=no-user-gesture-required",
                "--no-sandbox",
            ],
        )
        if self.ignore_ssl:
            context = self.browser.new_context(ignore_https_errors=True)
        else:
            context = self.browser.new_context()
        page = context.new_page()
        return page, context

    def connect_to_webi(self, url: str):
        """
        Connects to the web interface with the provided URL.

        Args:
            url (str): The URL of the web interface.
        """

        self.page.goto(url, wait_until="networkidle")
        self.page.fill("#it-login", self.login)
        self.page.fill("#it-password", self.pwd)
        self.page.click("#it-submit")
        self.page.wait_for_load_state("networkidle")
        self.page.goto(url, wait_until="networkidle")



    def check_for_errors(self):
        time.sleep(2)

        isError = self.page.evaluate(
            """
            () => {
                const tooltips = document.querySelector('#tooltips');
                return tooltips ? tooltips.innerText : null;
            }
        """
        )
        if isError:
            self.create_error_report(isError)

    def create_error_report(self, isError: str):
        logger.debug(f"{isError=}")
        xivo_errors: List[str] = self.page.evaluate(
            """
            () => {
                const errorList = Array.from(document.querySelectorAll('#report-xivo-error li'));
                return errorList.map(li => li.innerText);
            }
        """
        )
        logger.debug(f"xivo errors : {xivo_errors}")
        raise Exception(xivo_errors if xivo_errors else isError)

    def tearDown(self):
        self.page.close()
        self.browser.close()

    def get_number_of_pages(self, selector: str) -> int:
        """
        Returns the number of pages based on the given selector.

        Args:
            selector (str): The CSS selector to find the element containing the number of pages.

        Returns:
            int: The number of pages.
        """

        multiple_pages = self.page.query_selector(selector)
        if multiple_pages:
            number_of_pages_text = self.page.inner_text(selector)
            return int(number_of_pages_text)
        else:
            return 1

    def get_queue(self) -> Union[List[Dict[str, str]], None]:
        """
        Retrieves the list of queues from the web application.

        Format of queues_list if not None:
        queues_list = [{
            'id': '1',
            'name': 'myfirstqueue',
            'number': '40001',
            'displayName': 'myfirstqueue',
            }, {
            'id': '2',
            'name': 'mysecondqueue',
            'number': '40002',
            'displayName': 'mysecondqueue',
            }]

        Returns:
            Union[List[Dict[str, str]], None]: The list of queues or None if no queues found.
        """  

        self.page.goto(
            f"{self.base_url}/callcenter/index.php/settings/queues/?act=list",
            wait_until="networkidle",
        )
        page_content = self.page.text_content("#table-main-listing")

        if "No queue found" in page_content or "Aucune file d'attente" in page_content:
            return None

        number_of_pages = self.get_number_of_pages(
            "#s-asterisk > div > div:nth-child(1) > a:nth-last-child(2)"
        )

        queues_list: List[Dict[str, str]] = []
        for i in range(number_of_pages):
            page_url = f"{self.base_url}/callcenter/index.php/settings/queues/?act=list&page={i+1}"
            self.page.goto(page_url, wait_until="networkidle")

            self.page.query_selector_all(
                "#table-main-listing > tbody > tr:not(:first-child)"
            )
            queues_on_page: dict[str, str] = self.page.evaluate(
                """() => {
                const queues = document.querySelectorAll('#table-main-listing > tbody > tr:not(:first-child)');
                return Array.from(queues).map(queue => {
                    const idElement = queue.querySelector('td.td-right > a:first-child');
                    const nameElement = queue.querySelector('td.txt-left');
                    const numberElement = queue.querySelector('td:nth-child(3)');
                    const displayNameElement = queue.querySelector('td:nth-child(2) > label');
                    return {
                        id: idElement ? idElement.href.match(/(?:\\?|&)id=(\\d+)/)[1] : null,
                        name: nameElement ? nameElement.title : null,
                        number: numberElement ? numberElement.outerText : null,
                        displayName: displayNameElement ? displayNameElement.outerText : null
                    };
                });
            }"""
            )
            queues_list.extend(queues_on_page)

        # Format of queues_list :
        # queues_list = [
        #     {"id": "1", "name": "test", "number": "6060"},
        #     {"id": "2", "name": "test2", "number": "-"},
        # ]
        return queues_list

    def get_trunk(self) -> Union[List[Dict[str, str]], None]:
        """
        Retrieves the list of trunks from the web application.

        format of trunks_list if not None:
        trunks_list = [
            {"id": "3", "name": "qzfqqs", "host": "Unknown host"},
            {"id": "2", "name": "tse", "host": "Unknown host"},
        ]

        Returns:
            Union[List[Dict[str, str]], None]: The list of trunks or None if no trunks found.
        """

        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/trunk_management/sip",
            wait_until="networkidle",
        )

        page_content = self.page.text_content("#table-main-listing")
        if page_content and (
            "No SIP trunk found" in page_content
            or "Aucune interconnexion SIP" in page_content
        ):
            return None

        number_of_pages = self.get_number_of_pages(
            "#s-asterisk > div > div:nth-child(1) > a:nth-last-child(2)"
        )

        trunks_list: List[Dict[str, Any]] = []

        for i in range(number_of_pages):
            trunk_url = f"{self.base_url}/service/ipbx/index.php/trunk_management/sip/?act=list&page={i+1}"
            self.page.goto(trunk_url, wait_until="networkidle")
            self.page.query_selector_all(
                "#table-main-listing > tbody > tr:not(:first-child)"
            )
            trunks_on_page = self.page.evaluate(
                """() => {
                    const trunks = document.querySelectorAll('#table-main-listing > tbody > tr:not(:first-child)');
                    return Array.from(trunks).map(trunk => {
                        const idElement = trunk.querySelector('td.td-right > a:first-child');
                        const nameElement = trunk.querySelector('td.txt-left > label');
                        const hostElement = trunk.querySelector('td:nth-child(3)');  // Adjust if necessary
                        return {
                            id: idElement ? idElement.href.match(/(?:\\?|&)id=(\\d+)/)[1] : null,
                            name: nameElement ? nameElement.outerText : null,
                            host: hostElement ? hostElement.outerText : null
                        };
                    });
                }"""
            )
            trunks_list.extend(trunks_on_page)

        # Format of trunks_list :
        # trunks_list = [
        #     {"id": "3", "name": "qzfqqs", "host": "Unknown host"},
        #     {"id": "2", "name": "tse", "host": "Unknown host"},
        # ]

        return trunks_list

    def get_context(self) -> Union[List[Dict[str, str]], None]:
        """
        Retrieves the list of contexts from the web application.

        Format of contexts_list if not None:
        contexts_list = [
            {'id': 'myfirstcontext', 'name': 'myfirstcontext', 'type': 'Internal'}, 
            {'id': 'myfirstsubcontext', 'name': 'myfirstsubcontext', 'type': 'Incall'}
        ]

        Returns:
            Union[List[Dict[str, str]], None]: The list of contexts or None if no contexts found.
        """

        logger.info("Getting contexts from your XiVO")

        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/system_management/context",
            wait_until="networkidle",
        )

        page_content = self.page.text_content("#table-main-listing")
        if "No context found" in page_content or "Aucun context" in page_content:
            return None

        number_of_pages = self.get_number_of_pages(
            "#s-asterisk > div > div:nth-child(1) > a:nth-last-child(2)"
        )

        contexts_list = []
        for i in range(number_of_pages):
            if i > 0:
                self.page.goto(
                    f"{self.base_url}/service/ipbx/index.php/system_management/context/?act=list&page={i+1}",
                    wait_until="networkidle",
                )
            self.page.query_selector_all(
                "#table-main-listing > tbody > tr:not(:first-child)"
            )
            contexts_on_page = self.page.evaluate(
                """() => {
                const contexts = document.querySelectorAll('#table-main-listing > tbody > tr:not(:first-child)');
                return Array.from(contexts).map(context => {
                    const idElement = context.querySelector('td.td-right > a:first-child');
                    const nameElement = context.querySelector('td.txt-left > label');
                    const typeElement = context.querySelector('td:nth-child(4)');
                    return {
                        id: idElement ? (idElement.href.split('id=')[1] || '').split('&')[0] : null,
                        name: nameElement ? nameElement.outerText : null,
                        type: typeElement ? typeElement.title : null
                    };
                });
            }"""
            )
            contexts_list.extend(contexts_on_page)

        return contexts_list

    def get_incoming_call(self) -> Union[List[Dict[str, str]], None]:
        """
        Retrieves the list of incoming calls from the web application.

        Format of incomingCalls_list if not None:
        incomingCalls_list = [
            {'id': '933', 'did': '0101010101', 'destination': 'myfirstqueue 40001 [myfirstcontext]'},
            {'id': '934', 'did': '0101010102', 'destination': 'mysecondqueue 40002 [myfirstcontext]'}
        ]

        Returns:
            Union[List[Dict[str, str]], None]: The list of contexts or None if no incoming call found.
        """

        logger.info("Getting incoming calls from your XiVO")

        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/call_management/incall",
            wait_until="networkidle",
        )

        page_content = self.page.text_content("#table-main-listing")
        if (
            "No incoming call found" in page_content
            or "Aucun appel entrant" in page_content
        ):
            return None

        number_of_pages = self.get_number_of_pages(
            "#s-asterisk > div > div:nth-child(1) > a:nth-last-child(2)"
        )

        incoming_calls_list = []
        for i in range(number_of_pages):
            if i > 0:
                self.page.goto(
                    f"{self.base_url}/service/ipbx/index.php/call_management/incall/?act=list&page={i+1}",
                    wait_until="networkidle",
                )
            self.page.query_selector_all(
                "#table-main-listing > tbody > tr:not(:first-child)"
            )
            incoming_calls_on_page = self.page.evaluate(
                """() => {
                const incomingCalls = document.querySelectorAll('#table-main-listing > tbody > tr:not(:first-child)');
                return Array.from(incomingCalls).map(incomingCall => {
                    const idElement = incomingCall.querySelector('td.td-right > a:first-child');
                    const didElement = incomingCall.querySelector('td.txt-left > label');
                    const destinationElement = incomingCall.querySelector('td:nth-last-child(2)');
                    return {
                        id: idElement ? idElement.href.match(/(?:\?|&)id=(\d+)/)[1] : null,
                        did : didElement ? didElement.outerText : null,
                        destination: destinationElement ? destinationElement.title : null
                    };
                });
            }"""
            )
            incoming_calls_list.extend(incoming_calls_on_page)

        return incoming_calls_list

    def create_queue(self, queue: Dict[str, Any]):
        # Example of queue:
        # queue = {
        #     "name": "firstone",
        #     "display_name": "First One",
        #     "agents": {
        #         "context": "myfirstcontext",
        #         "user_prefix": "userQueueOne",
        #         "line_type": "sip",
        #         "first_number": "11100",
        #         "last_number": "11110",
        #         "mds": "default",
        #         "label": "Populator",
        #     },
        #     "number": "40001",
        #     "context": "default",
        # }
        logger.info(f"Creating queue {queue['name']}")
        logger.debug(f"{queue=}")

        self.page.goto(
            f"{self.base_url}/callcenter/index.php/settings/queues/?act=add",
            wait_until="networkidle",
        )
        self.page.evaluate(
            f"""() => {{
            document.querySelector('#it-queuefeatures-name').value = '{queue['name'].lower().replace(' ', '_')}'
            document.querySelector('#it-queuefeatures-displayname').value = '{queue['display_name']}'
            document.querySelector('#it-queuefeatures-number').value = '{queue['number']}'
            document.querySelector('#it-queuefeatures-context').value = '{queue['context']}'
            document.querySelector('#it-submit').click()
        }}"""
        )
        self.check_for_errors()

    def create_context(self, context: Dict[str, Any]):
        # Example of context:
        # context = {
        #     "name": "myfirstcontext",
        #     "display_name": "My First Context",
        #     "context_type": "internal",
        #     "include_sub_contexts": ["myfirstsubcontext"],
        #     "user": {"first_number": "10000", "last_number": "29999"},
        #     "group": {"first_number": "30000", "last_number": "39999"},
        #     "queue": {"first_number": 40000, "last_number": 49999},
        #     "meetme": {"first_number": 50000, "last_number": 59999},
        #     "incall": {"first_number": 60000, "last_number": 69999, "did_length": 5},
        # }
        logger.info(f"Creating context {context['name']}")
        logger.debug(f"{context=}")

        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/system_management/context/?act=add",
            wait_until="networkidle",
        )
        self.page.evaluate(
            f"""() => {{
                document.querySelector('#it-context-name').value = '{context['name']}'
                document.querySelector('#it-context-displayname').value = '{context['display_name']}'
                document.querySelector('#it-context-contexttype').value = '{context['context_type']}'
        }}"""
        )
        if context["include_sub_contexts"]:
            for subcontext in context["include_sub_contexts"]:
                self.page.evaluate(
                    f"""() => {{
                        document.querySelector('[title*="{subcontext}"] > a').click()

                }}"""
                )
        resources = ["user", "group", "queue", "meetme", "incall"]
        for resource in resources:
            if context[resource]["first_number"] and context[resource]["last_number"]:
                self.page.evaluate(
                    f"""() => {{
                        document.querySelectorAll('#add_line_button > img')[{resources.index(resource)}].click()
                        document.querySelector('#contextnumbers-{resource} > tr > td.td-left.txt-center > div > div > input').value = '{context[resource]['first_number']}'
                        document.querySelector('#contextnumbers-{resource} > tr > td:nth-child(2) > div > div > input').value = '{context[resource]['last_number']}'
                }}"""
                )
                if resource == "incall":
                    self.page.evaluate(
                        f"""() => {{
                            document.querySelector('#contextnumbers-incall > tr > td:nth-child(3) > div > div > select').value = '{context['incall']['did_length']}'
                    }}"""
                    )
        self.page.evaluate(
            f"""() => {{
                document.querySelector('#it-submit').click()
        }}"""
        )
        self.check_for_errors()

    def create_trunk(self, trunk: Dict[str, Any]):
        # Example of trunk:
        # trunk = {
        #     "name": "mytrunk",
        #     "authentification_username": "My Trunk",
        #     "password": "superpass",
        #     "caller_id": "mycallerid",
        #     "call_limit": "10",
        #     "connection_type": "peer",
        #     "ip_addressing_type": "static",
        #     "ip_address": "10.10.10.10",
        #     "context": "mycontext",
        #     "nat": "force_rport",
        #     "mds_id": "1"
        # }
        logger.info(f"Creating trunk {trunk['name']}")

        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/trunk_management/sip/?act=add",
            wait_until="networkidle",
        )
        time.sleep(3)
        self.page.evaluate(
            f"""() => {{
            document.querySelector('#it-protocol-name').value = '{trunk['name']}'
            document.querySelector('#it-protocol-username').value = '{trunk['authentification_username']}'
            document.querySelector('#it-protocol-secret').value = '{trunk['password']}'
            document.querySelector('#it-protocol-callerid').value = '{trunk['caller_id']}'
            document.querySelector('#it-protocol-calllimit').value = '{trunk['call_limit']}'
            document.querySelector('#it-protocol-type').value = '{trunk['connection_type']}'
        }}"""
        )
        if trunk["connection_type"].lower() != "user":
            self.page.evaluate(
                f"""() => {{
                document.querySelector('#it-protocol-host-type').value = '{trunk['ip_addressing_type']}'
            }}"""
            )
        if trunk["ip_addressing_type"].lower() == "static":
            self.page.evaluate(
                f"""() => {{
                document.querySelector('#fd-protocol-host-static').style.display = "block"
                document.querySelector('#it-protocol-host-static').disabled = false
                document.querySelector('#it-protocol-host-static').value = '{trunk['ip_address']}'
            }}"""
            )
        self.page.evaluate(
            f"""() => {{
            document.querySelector('#it-protocol-context').value = '{trunk['context']}'
            document.querySelector('#it-trunkfeatures-mediaserver').value = '{trunk['mds_id']}'
            document.querySelector('#it-protocol-nat').value = '{trunk['nat']}'
            document.querySelector('#it-submit').click()
        }}"""
        )
        self.check_for_errors()

    def create_incomingCall(self, incomingCall: Dict[str, Any], users_list, queues_list):
        # Example of incoming call:
        # incomingCall = {
        #     "did": "0101010101",
        #     "context": "default",
        #     "destination": "user",
        #     "custom_command": "null",
        #     "destination_number": "10002",
        #     "ring_time": "20",
        # }
        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/call_management/incall/?act=add",
            wait_until="networkidle",
        )
        self.page.evaluate(
            f"""() => {{
                document.querySelector('#it-incall-exten').value = '{incomingCall['did']}';
                document.querySelector('#it-incall-context').value = '{incomingCall['context']}';
                document.querySelector('#it-dialaction-answer-actiontype').value = '{incomingCall['destination']}';
        }}"""
        )
        if incomingCall["destination"] == "custom":
            self.page.evaluate(
                f"""() => {{
                document.querySelector('#fd-dialaction-answer-custom-actiontype').style.display = 'block';
                document.querySelector('#fd-dialaction-answer-custom-actionarg1').style.display = 'block';
                document.querySelector('#it-dialaction-answer-custom-actionarg1').disabled = false;
                document.querySelector('#it-dialaction-answer-custom-actionarg1').value = '{incomingCall['custom_command']}';
            }}"""
            )

        elif incomingCall['destination'] == "user" and users_list or incomingCall['destination'] == "queue" and queues_list:
            if incomingCall['destination'] == "user":
                destination_id = [user.id for user in users_list if user.mobile_phone_number == incomingCall['destination_number']][0]
            else:
                destination_id = [queue["id"] for queue in queues_list if queue["number"] == incomingCall["destination_number"]][0]
            self.page.evaluate(
                f"""() => {{
                document.querySelector('#fd-dialaction-answer-{incomingCall['destination']}-actiontype').style.display = 'block';
                document.querySelector('#fd-dialaction-answer-{incomingCall['destination']}-actionarg1').style.display = 'block';
                document.querySelector('#it-dialaction-answer-{incomingCall['destination']}-actionarg1').disabled = false;
                document.querySelector('#it-dialaction-answer-{incomingCall['destination']}-actionarg1').value = '{destination_id}';
                document.querySelector('#fd-dialaction-answer-{incomingCall['destination']}-actionarg2').style.display = 'block';
                document.querySelector('#it-dialaction-answer-{incomingCall['destination']}-actionarg2').disabled = false;
                document.querySelector('#it-dialaction-answer-{incomingCall['destination']}-actionarg2').value = '{incomingCall['ring_time']}';
            }}"""
            )

        self.page.evaluate(
            f"""() => {{
            document.querySelector('#it-submit').click();
        }}"""
        )
        self.check_for_errors()

    def delete_user(self, user_id: int):
        logger.info(f"Deleting user {user_id}")
        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/pbx_settings/users/?act=delete&id={user_id}"
        )

    def delete_agent(self, agent_group: int, agent_id: int):
        logger.info(f"Deleting agent {agent_id} from group {agent_group}")
        self.page.goto(
            f"{self.base_url}/callcenter/index.php/settings/agents/?act=deleteagent&group={agent_group}&id={agent_id}"
        )

    def delete_incomingCall(self, incomingCall_id: int):
        logger.info(f"Deleting incoming call {incomingCall_id}")
        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/call_management/incall/?act=delete&id={incomingCall_id}"
        )

    def delete_trunk(self, trunk_id: int):
        logger.info(f"Deleting trunk {trunk_id}")
        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/trunk_management/sip/?act=delete&id={trunk_id}"
        )


    def delete_context(self, context_id: str):
        """ Deletes a context from the web interface.

        Args:
            context_id (str): The name of the context to delete.
        """

        logger.info(f"Deleting context {context_id}")
        self.page.goto(
            f"{self.base_url}/service/ipbx/index.php/system_management/context/?act=delete&id={context_id}"
        )   

    def delete_queue(self, queue_id: int):
        """ Deletes a queue from the web interface.

        Args:
            queue_id (int): The ID of the queue to delete.
        """

        logger.info(f"Deleting queue {queue_id}")
        self.page.goto(
            f"{self.base_url}/callcenter/index.php/settings/queues/?act=delete&id={queue_id}"
        )
