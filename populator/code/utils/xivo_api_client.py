import requests
from requests.exceptions import SSLError
import logging
from typing import Optional, Dict, Any, List, Union, Type
import random
import string
import requests.exceptions
import inspect

from utils.browser_automation import BrowserAutomation
from utils.logger import setup_logger

from models.label import Label
from models.device import Device
from models.user import User
from models.agent import Agent
from models.line import Line
from models.extension import Extension


logger = setup_logger()

# pylint: disable=locally-disabled, logging-fstring-interpolation, broad-exception-raised, broad-exception-caught
# We can use fstring in logging if the function call in fstring isn't too big (ex: print variable)


class XiVOAPIClient:
    def __init__(
        self,
        base_url: str,
        webi_login: str,
        webi_pwd: str,
        ignore_ssl: bool = False,
        dry_run: bool = False,
    ):
        self.logger = logging.getLogger(__name__)
        self.ignore_ssl = ignore_ssl
        self.base_url = base_url
        self.webi_login = webi_login
        self.webi_pwd = webi_pwd
        self.__browser_automation = BrowserAutomation(
            base_url=base_url,
            login=webi_login,
            pwd=webi_pwd,
            ignore_ssl=ignore_ssl,
        )
        self.dry_run = dry_run

        if ignore_ssl:
            # Disable SSL warnings in case of self-signed certificates
            requests.packages.urllib3.disable_warnings()

    def _make_request(
        self,
        method: str,
        endpoint: str,
        params: Any = None,
        headers: Any = None,
        json: Any = None,
        data: Any = None,
        forced_url: str = "",
    ) -> requests.Response:
        url = f"{self.base_url + ':9486/1.1'}/{endpoint}"
        if forced_url:
            url = forced_url

        if (method == "DELETE" or method == "PUT" or method == "POST") and self.dry_run:
            logger.info(f"DRYRUN = {method} {url} {json=}")

            fake_response = requests.Response()
            if method == "PUT" or method == "DELETE":
                fake_response.status_code = 204
            elif method == "POST":
                fake_response.status_code = 201
            fake_response.json = lambda: {"message": "Fake response"}

            return fake_response

        response = None
        try:
            response = requests.request(
                method, url, params=params, headers=headers, json=json, data=data, verify=not (self.ignore_ssl)
            )

            return response
        except SSLError as e:
            # Handle SSL errors
            logger.error(f"SSL Error occurred: {e}\n")
            raise Exception(f"SSL Error occurred: {e}\n") from e
        except requests.exceptions.ConnectionError as e:
            logger.error(f"Connection Error occurred: {e}\n")
            raise Exception(f"Connection Error occurred: {e}\n") from e

    def _parse_response(
        self, response_data: Dict[str, Any], target_class: Type[Any]
    ) -> Any:
        valid_attributes = {
            k: v
            for k, v in response_data.items()
            if k in target_class.__init__.__code__.co_varnames
        }
        instance = target_class(**valid_attributes)
        return instance

    def _create_item(
        self, item_data: Dict[str, Any], endpoint: str, target_class: Type[Any]
    ):
        response = self._make_request("POST", endpoint, json=item_data)
        if response.status_code == 201:
            item = self._parse_response(response.json(), target_class)
            logger.debug(f"{endpoint} {item.id} created successfully")
            return item
        else:
            logger.error(
                f"Failed to create {endpoint}: {item_data} with status code {response.status_code}"
            )
            raise Exception(
                f"Failed to create {endpoint}: {item_data} with status code {response.status_code}"
            )

    def create_device(self, device_data: Dict[str, Any]) -> Device:
        return self._create_item(device_data, "devices", Device)

    def create_user(self, user_data: Dict[str, Any]) -> User:
        return self._create_item(user_data, "users", User)

    def create_agent(self, agent_data: Dict[str, Any]) -> Agent:
        return self._create_item(agent_data, "agents", Agent)

    def create_line(self, line_data: Dict[str, Any]) -> Line:
        return self._create_item(line_data, "lines", Line)

    def create_extension(self, extension_data: Dict[str, Any]) -> Extension:
        return self._create_item(extension_data, "extensions", Extension)

    def create_queue(self, queue: Dict[str, Any]):
        return self.__browser_automation.create_queue(queue)

    def create_context(self, context: Dict[str, Any]):
        return self.__browser_automation.create_context(context)

    def create_trunk(self, trunk: Dict[str, Any]):
        return self.__browser_automation.create_trunk(trunk)

    def create_incoming_call(
        self, incoming_call: Dict[str, Any], users: List[User], queues
    ):
        return self.__browser_automation.create_incomingCall(incoming_call, users, queues)

    def create_label(self, label_data: Dict[str, Any]):
        return self._create_item(label_data, "labels", Label)

    def _update_item(self, endpoint: str, item_id: int, resource_data: Dict[str, Any]):
        response = self._make_request(
            "PUT", f"{endpoint}/{item_id}", json=resource_data
        )
        if response.status_code == 204:
            # Since the response text is empty, we return a message instead
            # In perfect condition we should return the updated object
            logger.debug(f"{endpoint} {item_id} updated successfully")
            return f"message: {endpoint} {item_id} updated successfully"
        else:
            logger.error(
                f"Failed updating {endpoint} {item_id} with status code {response.status_code}\n {response.text=}"
            )
            exit()

    def update_device(self, device_id: int, device_data: Dict[str, Any]):
        return self._update_item("devices", device_id, device_data)

    def update_user(self, user_id: int, user_data: Dict[str, Any]):
        return self._update_item("users", user_id, user_data)

    def update_agent(self, agent_id: int, agent_data: Dict[str, Any]):
        return self._update_item("agents", agent_id, agent_data)

    def update_line(self, line_id: int, line_data: Dict[str, Any]):
        return self._update_item("lines", line_id, line_data)

    def update_label(self, label_id: int, label_data: Dict[str, Any]):
        return self._update_item("labels", label_id, label_data)

    def update_extension(self, extension_id: int, extension_data: Dict[str, Any]):
        return self._update_item("extensions", extension_id, extension_data)

    def _list_items(
        self, endpoint: str, target_class: Type[Any], **params: Any
    ) -> List[Any]:
        try:
            response = self._make_request("GET", endpoint, params=params)
            if response.status_code == 200:
                items_list: List[Any] = []
                for item in response.json()["items"]:
                    items_list.append(self._parse_response(item, target_class))
                logger.debug(f"List {endpoint} retrieved successfully")
                return items_list
            else:
                error_response = {
                    "error": f"Failed with status code {response.status_code}"
                }
                logger.error(error_response)
                raise Exception(error_response)
        except Exception as e:
            error_response = {"error": f"An error occurred: {str(e)}"}
            raise Exception(error_response) from e

    def list_agents(
        self,
        order: Optional[str] = None,
        direction: Optional[str] = None,
        limit: Optional[int] = None,
        offset: Optional[int] = None,
        search: Optional[str] = None,
    ) -> List[Agent]:
        return self._list_items(
            "agents",
            Agent,
            order=order,
            direction=direction,
            limit=limit,
            offset=offset,
            search=search,
        )

    def list_users(
        self,
        order: str = None,
        direction: str = None,
        limit: int = None,
        offset: int = None,
        search: str = None,
        view: str = None,
    ) -> List[User]:
        return self._list_items(
            "users",
            User,
            order=order,
            direction=direction,
            limit=limit,
            offset=offset,
            search=search,
            view=view,
        )

    def list_devices(
        self,
        order: str = None,
        direction: str = None,
        limit: int = None,
        offset: int = None,
        search: str = None,
    ) -> List[Device]:
        return self._list_items(
            "devices",
            Device,
            order=order,
            direction=direction,
            limit=limit,
            offset=offset,
            search=search,
        )

    def list_lines(
        self,
        order: str = None,
        direction: str = None,
        limit: int = None,
        offset: int = None,
        search: str = None,
    ) -> List[Line]:
        return self._list_items(
            "lines",
            Line,
            orfer=order,
            direction=direction,
            limit=limit,
            offset=offset,
            search=search,
        )

    def list_extensions(
        self,
        order: str = None,
        direction: str = None,
        limit: int = None,
        offset: int = None,
        search: str = None,
    ) -> List[Extension]:
        return self._list_items(
            "extensions",
            Extension,
            order=order,
            direction=direction,
            limit=limit,
            offset=offset,
            search=search,
        )

    def list_queues(self):
        logger.debug("Getting queues")
        queues = self.__browser_automation.get_queue()
        logger.debug(f"{queues=}")
        return queues if queues else []

    def list_trunks(self):
        trunks = self.__browser_automation.get_trunk()
        logger.debug(f"{trunks=}")
        return trunks if trunks else []

    def list_contexts(self):
        contexts = self.__browser_automation.get_context()
        logger.debug(f"{contexts=}")
        return contexts if contexts else []

    def list_incoming_calls(self):
        incoming_calls = self.__browser_automation.get_incoming_call()
        logger.debug(f"{incoming_calls=}")
        return incoming_calls if incoming_calls else []

    def list_labels(self, limit: int = None, search: str = None) -> List[Label]:
        params = {}
        if limit is not None:
            params["limit"] = limit
        if search is not None:
            params["search"] = search

        return self._list_items("labels", Label, **params)

    def _get_item(self, item_id: int, endpoint: str, target_class: Type[Any]):
        response = self._make_request("GET", f"{endpoint}/{item_id}")
        if response.status_code == 200:
            item = self._parse_response(response.json(), target_class)
            logger.debug(f"{target_class.__name__} {item_id} retrieved successfully")
            return item
        elif response.status_code == 404:
            logger.error(
                f"ERROR: while retrieving {target_class.__name__} with id {item_id}"
            )
            exit()
        else:
            logger.error(f"Failed with status code {response.status_code}")
            exit()

    def get_device(self, device_id: int) -> Device:
        return self._get_item(device_id, "devices", Device)

    def get_user(self, user_id: int) -> User:
        return self._get_item(user_id, "users", User)

    def get_agent(self, agent_id: int) -> Agent:
        return self._get_item(agent_id, "agents", Agent)

    def get_line(self, line_id: int) -> Line:
        return self._get_item(line_id, "lines", Line)

    def get_label(self, label_id: int):
        return self._get_item(label_id, "labels", Label)

    def get_extension(self, extension_id: int):
        return self._get_item(extension_id, "extensions", Extension)

    def _delete_item(self, item_id: Union[str, int], endpoint: str) -> str:
        response = self._make_request("DELETE", f"{endpoint}/{item_id}")
        if response.status_code == 204:
            # Since the response text is empty, we return a message instead
            # In perfect condition we should return the updated object
            logger.debug(f"{endpoint} {item_id} deleted successfully")
            return f"message: {endpoint} {item_id} deleted successfully"
        else:
            logger.debug(
                f"Failed deleting {endpoint} {item_id} with status code {response.status_code}\n {response.text=}"
            )
            raise Exception(
                f"Failed deleting {endpoint} {item_id} with status code {response.status_code}\n : {response.text}"
            )

    def delete_agent(self, agent_id: int):
        return self._delete_item(agent_id, "agents")

    def delete_user(self, user_id: int):
        return self._delete_item(user_id, "users")

    def delete_device(self, device_id: str):
        return self._delete_item(device_id, "devices")

    def delete_line(self, line_id: int):
        return self._delete_item(line_id, "lines")

    def delete_label(self, label_id: int):
        return self._delete_item(label_id, "labels")

    def delete_extension(self, extension_id: int):
        try:
            return self._delete_item(extension_id, "extensions")
        except Exception as e:
            if "Context was not found" in str(e) and "xivo-features" in str(e):
                pass
                # Cant delete extension that from xivo-features context
            else:
                raise

    def import_users_csv(self, csv_data: str) -> requests.Response:
        url = f"{self.base_url}:9486/1.1/users/import"
        headers = {"Content-Type": "text/csv; charset=utf-8"}

        response = self._make_request("POST", endpoint="users/import", headers=headers, data=csv_data)

        if response.status_code == 201:
            self.logger.debug("Users in csv imported successfully")
            return response
        elif response.status_code == 400:
            self.logger.error("Errors occurred during import")
            if "is outside of range for context" in response.text:
                self.logger.error(
                    f"Error while importing user : The range of extensions is outside of the context range.\n {response.text=}"
                )
                raise Exception(
                    "Error while importing user : The range of extensions is outside of the context range."
                )
            elif "User already exists" in response.text:
                self.logger.error(
                    f"Error while importing user : user in your csv already exists.\n {response.text=}"
                )
                raise Exception(
                    "Error while importing user : user in your csv already exists."
                )
            elif "Context was not found" in response.text:
                self.logger.error(
                    f"Error while importing user : Context was not found.\n {response.text=}"
                )
                raise Exception("Error while importing user : Context was not found.")
            elif "Registrar was not found" in response.text:
                self.logger.error(
                    "Error while importing user : MDS was not found.\n {response.text=}"
                )
                raise Exception("Error while importing user : MDS was not found.")
            elif (
                "User must have a username and password to enable a CtiProfile"
                in response.text
            ):
                self.logger.error(
                    f"Error while importing user : Either Username or Password was not found.\n {response.text=}"
                )
                raise Exception(
                    "Error while importing user : Either Username or Password was not found."
                )
            elif "Input Error - field 'labels':" in response.text:
                self.logger.error(
                    f"Error while importing user : labels was not found. You probably need to create the label first.\n {response.text=}"
                )
                raise Exception(
                    "Error while importing user : labels was not found. You probably need to create the label first."
                )
            else:
                raise Exception(
                    f"Error while importing user : Errors occurred during import. \n {response.text=}"
                )
        else:
            self.logger.error(
                f"Failed to import users. Status code {response.status_code}"
            )
            raise Exception(
                f"import_users_csv failed with status code {response.status_code}"
            )

    def export_csv(self) -> requests.Response:
        return self._make_request(
            "GET", "users/export", params={"content-type": "text/csv"}
        )
    
    def export_csv_json(self) -> requests.Response:
        return self._make_request(
            "GET", "users/export", headers={"Content-Type": "application/json", "Accept": "application/json"}
        )

    def associate_line_device(self, line_id: str, device_id: str) -> None:
        endpoint = f"lines/{line_id}/devices/{device_id}"
        response = self._make_request("PUT", endpoint)
        if response.status_code == 204:
            self.logger.debug(
                f"Associated line {line_id} with device {device_id} successfully"
            )
        elif response.status_code == 400:
            self.logger.error(
                f"Failed to associate line {line_id} with device {device_id}. Bad request."
            )
            raise Exception(
                "Failed to associate line {line_id} with device {device_id}. Bad request."
            )
        elif response.status_code == 404:
            self.logger.error(f"Line {line_id} or device {device_id} not found.")
            raise Exception(f"Line {line_id} or device {device_id} not found.")
        else:
            self.logger.error(
                f"Error while associating line : {line_id} with device {device_id}. Status code {response.status_code}"
            )
            raise Exception(
                f"Error while associating line : {line_id} with device {device_id}. Status code {response.status_code}"
            )

    def populate_queue_agent(self, queue_id: int, agent_id: str) -> None:
        endpoint = f"queues/{queue_id}/members/agents"
        payload = {"agent_id": agent_id, "queue_id": queue_id, "penalty": 0}
        response = self._make_request("POST", endpoint, json=payload)
        if response.status_code == 201:
            logger.debug(f"Added agent {agent_id} to queue {queue_id} successfully")
        elif response.status_code == 400:
            logger.error(
                f"Failed to add agent {agent_id} to queue {queue_id}. Bad request."
            )
            raise Exception(
                f"Failed to add agent {agent_id} to queue {queue_id}. Bad request."
            )
        elif response.status_code == 404:
            logger.error(f"Agent {agent_id} or queue {queue_id} not found.")
            raise Exception(f"Agent {agent_id} or queue {queue_id} not found.")
        elif response.status_code == 500:
            if response.text == "already exist":
                return
        else:
            logger.error(
                f"Error while Adding agent : {agent_id} to queue {queue_id}. Status code {response.status_code}"
            )
            raise Exception(
                f"Error while Adding agent : {agent_id} to queue {queue_id}. Status code {response.status_code}"
            )

    def get_live_reload_status(self) -> bool:
        function_name = inspect.currentframe().f_code.co_name
        try:
            response = self._make_request("GET", "configuration/live_reload")
            if response.status_code == 200:
                if response.json()["enabled"]:
                    self.logger.debug(f"{function_name} : Live reload is enabled")
                    return True
                else:
                    self.logger.debug(f"{function_name} : Live reload is disabled")
                    return False
            else:
                self.logger.error(
                    f"{function_name} : Failed with status code {response.status_code}"
                )
                raise Exception(
                    f"{function_name} : Failed with status code {response.status_code}"
                )
        except Exception as e:
            self.logger.error(
                f"{function_name} : Failed to get live reload status : {e}"
            )
            raise Exception(
                f"{function_name} : Failed to get live reload status : {e}"
            ) from e

    def update_live_reload_status(self, live_reload: bool) -> None:
        function_name = inspect.currentframe().f_code.co_name
        data = {
            "enabled": live_reload,
        }
        try:
            response = self._make_request("PUT", "configuration/live_reload", json=data)
            if response.status_code == 204:
                self.logger.info(
                    f"{function_name} : Live reload status updated successfully to {live_reload}"
                )
            else:
                self.logger.error(
                    f"{function_name} : update_live_reload_status failed with status code {response.status_code}"
                )
                raise Exception(
                    f"{function_name} : Failed to update live reload status (HTTP {response.status_code})"
                )
        except Exception as e:
            self.logger.error(
                f"{function_name} : Failed to update live reload status : {e}"
            )
            raise Exception(
                f"{function_name} : Failed to update live reload status : {e}"
            ) from e

    def get_agent_queues(self, agent_id: int) -> List[Dict[str, Any]]:
        endpoint = f"agents/{agent_id}/queues"
        try:
            response = self._make_request("GET", endpoint)
            if response.status_code == 200:
                self.logger.debug(f"{endpoint} {agent_id} retrieved successfully")
                return response.json()
            else:
                self.logger.error(f"Failed with status code {response.status_code}")
                return response.json()
        except Exception as e:
            self.logger.error(f"An error occurred: {str(e)}")
            raise Exception(f"An error occurred: {str(e)}")

    def get_agent_queue_association(self, queue_id: int, agent_id: int):
        endpoint = f"queues/{queue_id}/members/agents/{agent_id}"
        try:
            response = self._make_request("GET", endpoint)
            if response.status_code == 200:
                self.logger.debug(f"{endpoint} {agent_id} retrieved successfully")
                return response.json()
            else:
                self.logger.error(f"Failed with status code {response.status_code}")
                return response.json()
        except Exception as e:
            self.logger.error(f"An error occurred: {str(e)}")
            raise Exception(f"An error occurred: {str(e)}")

    def delete_queue_agent_association(self, queue_id: int, agent_id: int):
        endpoint = f"queues/{queue_id}/members/agents/{agent_id}"
        try:
            response = self._make_request("DELETE", endpoint)
            if response.status_code == 204:
                self.logger.debug(f"{endpoint} {agent_id} deleted successfully")
            else:
                self.logger.error(f"Failed with status code {response.status_code}")
        except Exception as e:
            self.logger.error(f"An error occurred: aze{str(e)}")
            raise Exception(f"An error occurred: {str(e)}")

    def delete_line_device_association(self, line_id: int, device_id: str):
        endpoint = f"lines/{line_id}/devices/{device_id}"
        response = self._make_request("DELETE", endpoint)
        if response.status_code == 204:
            self.logger.debug(f"{endpoint} {line_id} deleted successfully")
        else:
            self.logger.error(f"Failed with status code {response.status_code}")
            self.logger.error(f"{response.text=}")
            raise Exception(f"Failed with status code {response.status_code}")

    def delete_line_user_association(self, line_id: int, user_id: int):
        endpoint = f"users/{user_id}/lines/{line_id}"
        try:
            response = self._make_request("DELETE", endpoint)
            if response.status_code == 204:
                self.logger.debug(f"message: {endpoint} {line_id} deleted successfully")
            else:
                self.logger.error(f"Failed with status code {response.status_code}")
        except Exception as e:
            self.logger.error(
                f"An error occurred in delete_line_user_association: {str(e)}"
            )
            raise Exception(
                f"An error occurred in delete_line_user_association: {str(e)}"
            )

    def delete_line_extension_association(self, line_id: int, extension_id: int):
        endpoint = f"lines/{line_id}/extensions/{extension_id}"
        try:
            response = self._make_request("DELETE", endpoint)
            if response.status_code == 204:
                self.logger.debug(f"{endpoint} {line_id} deleted successfully")
            else:
                self.logger.error(f"Failed with status code {response.status_code}")
        except Exception as e:
            self.logger.error(f"An error occurred: {str(e)}")
            raise Exception(f"An error occurred: {str(e)}")

    def delete_queue(self, queue_id: int):
        if self.dry_run:
            logger.info(f"DRYRUN = delete_queue {queue_id=}")
            return
        
        return self.__browser_automation.delete_queue(queue_id)

    def delete_user_webi(self, user_id: int):
        if self.dry_run:
            logger.info(f"DRYRUN = delete_user_webi {user_id=}")
            return

        return self.__browser_automation.delete_user(user_id)

    def delete_agent_webi(self, agent_group: int, agent_id: int):
        if self.dry_run:
            logger.info(f"DRYRUN = delete_agent_webi {agent_group=} {agent_id=}")
            return

        return self.__browser_automation.delete_agent(agent_group, agent_id)

    def delete_incomingCall(self, incomingCall_id: int):
        if self.dry_run:
            logger.info(f"DRYRUN = delete_incomingCall {incomingCall_id=}")
            return

        return self.__browser_automation.delete_incomingCall(incomingCall_id)

    def delete_trunk(self, trunk_id: int):
        if self.dry_run:
            logger.info(f"DRYRUN = delete_trunk {trunk_id=}")
            return

        return self.__browser_automation.delete_trunk(trunk_id)

    def delete_context(self, context_id: str):
        if self.dry_run:
            logger.info(f"DRYRUN = delete_context {context_id=}")
            return

        return self.__browser_automation.delete_context(context_id)


def main():
    # # Example usage
    # client = XiVOAPIClient(base_url="https://ajm1-xivo.ajmwisper.fr", ignore_ssl=True)

    def print_first_five_users():
        users_list = client.list_users()
        logger.info(users_list)
        i = 0
        for user in users_list:
            logger.info(user)
            i += 1
            if i == 5:
                break

    def print_live_reload_status():
        # Update live reload status
        client.update_live_reload_status(False)

        live_reload_status = client.get_live_reload_status()
        logger.info(f"live_reload_status : {live_reload_status}")

        # Update live reload status
        client.update_live_reload_status(True)

    def print_first_five_devices():
        devices_list = client.list_devices()
        i = 0
        for device in devices_list:
            logger.info(device)
            i += 1
            if i == 5:
                break

    def print_first_five_agents():
        i = 0
        for agent in client.list_agents():
            logger.info(agent)
            i += 1
            if i == 5:
                break

    def generate_random_string(length):
        letters = string.ascii_lowercase
        return "".join(random.choice(letters) for _ in range(length))

    def create_get_update_get_delete_device():
        # Create a device
        new_device_data = {  # Replace this with actual device data
            "name": "New Device",
            # Other device properties...
        }
        created_device = client.create_device(new_device_data)
        logger.info(f"{created_device=}")

        # Get a device by ID
        device_info = client.get_device(created_device.id)
        logger.info(f"{device_info=}")

        # Update a device
        updated_device_data = {  # Replace this with updated device data
            "name": "Updated Device",
            # Other updated device properties...
        }
        updated_device = client.update_device(created_device.id, updated_device_data)
        logger.info(f"{updated_device=}")

        # Delete a device by ID
        delete_response = client.delete_device(created_device.id)
        logger.info(f"{delete_response=}")

    def create_get_update_get_delete_user():
        new_user_data = {
            "username": generate_random_string(10),
            "password": generate_random_string(10),
            # "email": generate_random_string(10) + "@example.com",
            "firstname": generate_random_string(5),
            # "lastname": generate_random_string(5),
            # Other user data...
        }
        user_created = client.create_user(new_user_data)
        logger.info(f"{user_created=}")

        user_info = client.get_user(user_created.id)
        logger.info(f"{user_info=}")
        # Update user information
        updated_user_data = {
            "username": "updated_" + user_created.username,
            # "email": "updated_" + user_created.email,
            "firstname": "Updated" + user_created.firstname,
            # "lastname": "User" + user_created.lastname,
            # Other updated user data...
        }
        update_user_response = client.update_user(user_created.id, updated_user_data)
        logger.info(
            f"{update_user_response}"
        )  # Print the response after updating the user
        logger.info(f"{client.get_user(user_created.id)}")

        # Delete the user
        delete_user_response = client.delete_user(user_created.id)
        logger.info(
            f"{delete_user_response=}"
        )  # Print the response after deleting the user

    def create_get_update_get_delete_agent():
        new_agent_data = {
            "firstname": generate_random_string(10),
            "lastname": generate_random_string(10),
            "username": generate_random_string(10),
            "password": "password123",
            "language": "fr_FR",
            "number": str(random.randint(1000, 1999)),
            "enabled": True,
            # Add other required fields based on your API specifications
        }

        # Creating a new agent using the create_agent method
        agent_created = client.create_agent(new_agent_data)
        logger.info(f"{agent_created=}")

        # Get agent information
        agent_info = client.get_agent(agent_created.id)

        # Update agent information
        updated_agent_data = {
            "firstname": "updated_" + agent_created.firstname,
            "lastname": "updated_" + agent_created.lastname,
            "Password": "password1234",
            # Add other required fields based on your API specifications
        }

        update_agent_response = client.update_agent(
            agent_created.id, updated_agent_data
        )
        logger.info(f"{update_agent_response=}")

        # Delete the agent
        delete_agent_response = client.delete_agent(agent_created.id)
        logger.info(f"{delete_agent_response=}")

        print_first_five_agents()

    def create_get_update_get_delete_line():
        new_line_data = {
            "name": generate_random_string(10),
            "device_id": "34409273cfa54747bcb1904e54e6f415",
            "device_slot": "1",
            "context": "default",
            "protocol": "sip",
            # Add other required fields based on your API specifications
        }
        line_created = client.create_line(new_line_data)
        logger.info(f"{line_created=}")

        line_info = client.get_line(line_created.id)
        logger.info(f"{line_info=}")

        updated_line_data = {
            "name": "Updated Line",
            # Add other required fields based on your API specifications
        }

        update_line_response = client.update_line(line_created.id, updated_line_data)
        logger.info(f"{update_line_response=}")

        delete_line_response = client.delete_line(line_created.id)
        logger.info(f"{delete_line_response=}")

    # print_live_reload_status()
    # exit()
    # print_first_five_users()
    # print_live_reload_status()
    # print_first_five_devices()
    # print_first_five_agents()

    # create_get_update_get_delete_device()
    # create_get_update_get_delete_user()
    # create_get_update_get_delete_agent()


if __name__ == "__main__":
    main()
