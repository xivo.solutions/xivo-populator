import yaml
from typing import List, Any, Dict, Union
import os

from utils.logger import setup_logger

logger = setup_logger()

# pylint: disable=locally-disabled, logging-fstring-interpolation
# We can use fstring in logging if the function call in fstring isn't too big (ex: print variable)

class YamlLoader:
    def __init__(self, config_dir: str = 'config') -> None:
        self.config_dir: str = config_dir

    def load(
        self, 
        file_name: str, 
        path_node: List[str], 
        yaml_dictionary: Dict[str, Any] = {}, 
        path_accumulator: str = ""
    ) -> Union[Dict[str, Any], Any]:
        if path_accumulator:
            path_accumulator += " -> " + path_node[0]
        else:
            path_accumulator = path_node[0]

        if not yaml_dictionary:
            file_path = os.path.join(self.config_dir, file_name)
            try:
                with open(file_path, encoding="UTF-8") as f:
                    yaml_dictionary = yaml.safe_load(f)
            except FileNotFoundError as e:
                logger.exception(f"Exception {e} raised; file_path: {file_path}")
                raise SystemExit

        try:
            return (
                self.load(file_name, path_node[1:], yaml_dictionary[path_node[0]], path_accumulator)
                if len(path_node) > 1
                else yaml_dictionary[path_node[0]]
            )
        except KeyError as e:
            raise KeyError(f"KeyError {e} raised | While trying to access full path '{path_accumulator}'") from e
