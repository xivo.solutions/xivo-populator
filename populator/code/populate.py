import concurrent.futures
from typing import List, Any, Dict

from utils.generators.device_generator import create_devices
from utils.logger import setup_logger
from models.device import Device
from models.extension import Extension
from models.line import Line
from models.user import User
from models.agent import Agent
from utils.xivo_api_client import XiVOAPIClient, User
from utils.yaml_loader import YamlLoader
from utils.agents_config import create_agents
from utils.generators import create_users

logger = setup_logger()

# pylint: disable=locally-disabled, logging-fstring-interpolation
# We can use fstring in logging if the function call in fstring isn't too big (ex: print variable)


class Populate:
    def __init__(self) -> None:
        logger.debug("Initializing Populate class...")
        self.yaml_loader: YamlLoader = YamlLoader()
        self.dry_run: bool = (
            True
            if self.yaml_loader.load("config.yml", ["specials"]).lower() == "dry"
            else False
        )
        self.xivo_client: XiVOAPIClient = XiVOAPIClient(
            base_url=f"https://{self.yaml_loader.load('config.yml', ['credentials', 'xivo_ip'])}",
            webi_login=self.yaml_loader.load(
                "config.yml", ["credentials", "webi_login"]
            ),
            webi_pwd=self.yaml_loader.load("config.yml", ["credentials", "webi_pwd"]),
            ignore_ssl=True,
            dry_run=self.dry_run,
        )
        if self.dry_run:
            logger.info("Dry run mode enabled")

    def _delete_user(
        self, number_to_delete: List[int] = None, delete_all: bool = False
    ) -> None:
        def delete_association(
            xivo_lines: List[Line],
            xivo_users: List[User],
            xivo_extensions: List[Extension],
        ) -> None:
            def process_delete_association(xivo_line: Line) -> None:
                # Delete association between line and device
                if xivo_line.device_id:
                    self.xivo_client.delete_line_device_association(
                        xivo_line.id, xivo_line.device_id
                    )
                    logger.debug(
                        f"deleted association between line {xivo_line.id}:{xivo_line.caller_id_num} and device {xivo_line.device_id}"
                    )

                # Delete association between line and user
                for xivo_user in xivo_users:
                    if xivo_user.username == xivo_line.caller_id_num:
                        self.xivo_client.delete_line_user_association(
                            xivo_line.id, int(xivo_user.id)
                        )
                        logger.debug(
                            f"deleted association between line {xivo_line.id} and user {xivo_user.id}"
                        )

                # Delete association between line and extension
                for xivo_extension in xivo_extensions:
                    try:
                        if int(xivo_extension.exten) == int(xivo_line.caller_id_num):
                            self.xivo_client.delete_line_extension_association(
                                xivo_line.id, xivo_extension.id
                            )
                            logger.debug(
                                f"deleted association between line {xivo_line.id}:{xivo_line.caller_id_num} and extension {xivo_extension.id}:{xivo_extension.exten}"
                            )
                            break
                    except Exception as e:
                        pass

                # Finally, delete the line
                self.xivo_client.delete_line(xivo_line.id)
                logger.debug(f"deleted line {xivo_line.caller_id_num}")

            # Using ThreadPoolExecutor to speed up process
            with concurrent.futures.ThreadPoolExecutor() as executor:
                futures = [
                    executor.submit(process_delete_association, xivo_line)
                    for xivo_line in xivo_lines
                ]

                # Waiting for all tasks to be completed
                for future in concurrent.futures.as_completed(futures):
                    try:
                        future.result()
                    except Exception as e:
                        logger.exception(f"Error while deleting line: {e}")
                        raise Exception(f"Error while deleting line: {e}") from e

        def __delete_device(xivo_devices: List[Device]):
            def process_delete_device(xivo_device: Device):
                self.xivo_client.delete_device(xivo_device.id)
                logger.debug(f"deleted device {xivo_device.mac}")

            # Using ThreadPoolExecutor to speed up process
            with concurrent.futures.ThreadPoolExecutor() as executor:
                futures = [
                    executor.submit(process_delete_device, xivo_device)
                    for xivo_device in xivo_devices
                ]

                # Waiting for all tasks to be completed
                for future in concurrent.futures.as_completed(futures):
                    try:
                        future.result()
                    except Exception as e:
                        logger.exception(f"Error while deleting device: {e}")

        def __delete_extension(xivo_extensions: List[Extension]):
            def process_delete_extension(xivo_extension: Extension):
                if xivo_extension.id:
                    self.xivo_client.delete_extension(xivo_extension.id)
                    logger.debug(f"deleted extension {xivo_extension.exten}")

            # Using ThreadPoolExecutor to speed up process
            with concurrent.futures.ThreadPoolExecutor() as executor:
                futures = [
                    executor.submit(process_delete_extension, xivo_extension)
                    for xivo_extension in xivo_extensions
                    # if xivo_extension.context != "xivo-features"
                ]

                # Waiting for all tasks to be completed
                for future in concurrent.futures.as_completed(futures):
                    try:
                        future.result()
                    except Exception as e:
                        if delete_all:
                            if "Extension is associated with a queue" in str(e):
                                pass
                            elif "Extension is associated with a incall" in str(e):
                                pass
                            # both of these errors are expected because of delete_all agressive behavior, it's fine
                            else:
                                logger.error(f"Error while deleting extension: {e}")
                        else:
                            logger.exception(f"Error while deleting extension: {e}")

        def __delete_user(xivo_users: List[User]):
            def process_delete_user(xivo_user: User):
                self.xivo_client.delete_user(xivo_user.id)
                logger.debug(f"deleted user {xivo_user.mobile_phone_number}")

            # Using ThreadPoolExecutor to speed up process
            with concurrent.futures.ThreadPoolExecutor() as executor:
                futures = [
                    executor.submit(process_delete_user, xivo_user)
                    for xivo_user in xivo_users
                    if xivo_user.username != "xuc"
                ]

                # Waiting for all tasks to be completed
                for future in concurrent.futures.as_completed(futures):
                    try:
                        future.result()
                    except Exception as e:
                        logger.exception(f"Error while deleting user: {e}")

        def run_deletes(
            xivo_devices: List[Device],
            xivo_extensions: List[Extension],
            xivo_users: List[User],
        ):
            with concurrent.futures.ThreadPoolExecutor() as executor:
                device_future = executor.submit(__delete_device, xivo_devices)
                extension_future = executor.submit(__delete_extension, xivo_extensions)
                user_future = executor.submit(__delete_user, xivo_users)

                # Wait for all deletes to complete
                for future in concurrent.futures.as_completed(
                    [device_future, extension_future, user_future]
                ):
                    try:
                        future.result()
                    except Exception as e:
                        logger.exception(f"Error in delete operation: {e}")

        xivo_extensions = self.xivo_client.list_extensions()
        xivo_lines = self.xivo_client.list_lines()
        xivo_users = self.xivo_client.list_users()
        xivo_devices = self.xivo_client.list_devices()

        if delete_all:
            # Delete all users, devices, extensions, and their associations
            delete_association(xivo_lines, xivo_users, xivo_extensions)
            run_deletes(xivo_devices, xivo_extensions, xivo_users)
        else:
            # Delete users, devices, extensions, and their associations based on number_to_delete
            filtered_lines = [
                line
                for line in xivo_lines
                if line.caller_id_num and int(line.caller_id_num) in number_to_delete
            ]
            filtered_users = [
                user
                for user in xivo_users
                if user.mobile_phone_number
                and int(user.mobile_phone_number) in number_to_delete
            ]
            filtered_extensions = [
                extension
                for extension in xivo_extensions
                if extension.exten.isdigit()
                and int(extension.exten) in number_to_delete
            ]
            filtered_devices = [
                device
                for device in xivo_devices
                if int(device.mac.replace(":", "")) - 500000000000 in number_to_delete
            ]

            delete_association(filtered_lines, filtered_users, filtered_extensions)
            run_deletes(filtered_devices, filtered_extensions, filtered_users)

    def delete_users(self):
        """Delete users part of the config.yml"""

        try:
            yaml_users = self.yaml_loader.load("config.yml", ["users"])
        except KeyError:
            logger.warning("No users found in config.yml, 0 user will be deleted")
            return

        logger.info("Start deleting users part of the config.yml...")

        number_to_delete: List[int] = []
        for yaml_user in yaml_users:
            number_to_delete.extend(
                range(
                    int(
                        self.yaml_loader.load(
                            "config.yml",
                            ["users", f"{yaml_user}", "first_number"],
                        )
                    ),
                    int(
                        self.yaml_loader.load(
                            "config.yml",
                            ["users", f"{yaml_user}", "last_number"],
                        )
                    )
                    + 1,
                )
            )

        self._delete_user(number_to_delete)

        logger.info("Finished deleting users part of the config.yml")

    def delete_queues(self):
        """Delete queues part of the config.yml"""
        try:
            yaml_queues = self.yaml_loader.load("config.yml", ["queues"])
        except KeyError:
            logger.warning("No queues found in config.yml, 0 queue will be deleted")
            return

        logger.info("Start deleting queues part of the config.yml...")

        number_to_delete: List[int] = []
        for yaml_queue in yaml_queues:
            if "agents" in yaml_queues[yaml_queue]:
                number_to_delete.extend(
                    range(
                        int(
                            self.yaml_loader.load(
                                "config.yml",
                                ["queues", f"{yaml_queue}", "agents", "first_number"],
                            )
                        ),
                        int(
                            self.yaml_loader.load(
                                "config.yml",
                                ["queues", f"{yaml_queue}", "agents", "last_number"],
                            )
                        )
                        + 1,
                    )
                )
        queues_name = [yaml_queues[yaml_queue]['name'].lower().replace(' ', '_') for yaml_queue in yaml_queues]

        self._delete_queues(queues_name, number_to_delete)

        logger.info("Finished deleting queues part of the config.yml")

    def _delete_queues(self, queues_name : list[str] = None, numbers_to_delete: List[int] = None, delete_all: bool = False):
        def delete_queues(queues: List[Dict]):
            for queue in queues:
                try:
                    self.xivo_client.delete_queue(int(queue["id"]))
                    logger.debug(f"Deleted queue {queue['name']}")
                except Exception as e:
                    logger.error(f"Error deleting queue {queue['name']}: {e}")

        def delete_agents(agents: List[Agent]):
            for agent in agents:
                try:
                    self.xivo_client.delete_agent(agent.id)
                    logger.debug(f"Deleted agent {agent.number}")
                except Exception as e:
                    logger.error(f"Error deleting agent {agent.number}: {e}")

        xivo_queues = self.xivo_client.list_queues()
        xivo_agents = self.xivo_client.list_agents()

        if delete_all:
            queues_to_delete = xivo_queues
            agents_to_delete = xivo_agents
        else:
            queues_to_delete = [queue for queue in xivo_queues if queue["name"] in queues_name]
            agents_to_delete = [agent for agent in xivo_agents if int(agent.number) in numbers_to_delete]

        delete_queues(queues_to_delete)
        delete_agents(agents_to_delete)
        if numbers_to_delete:
            self._delete_user(numbers_to_delete)


    def delete_incoming_calls(self):
        try:
            yaml_incoming_calls = self.yaml_loader.load("config.yml", ["incomingCalls"])
        except KeyError:
            logger.warning(
                "No incomingCalls found in config.yml, 0 incomingCalls will be deleted"
            )
            return

        logger.info("Start deleting the incoming calls...")

        xivo_incoming_calls = self.xivo_client.list_incoming_calls()
        logger.debug(f"{xivo_incoming_calls=}")
        if xivo_incoming_calls:
            for yaml_incoming_call in yaml_incoming_calls:
                for xivo_incoming_call in xivo_incoming_calls:
                    xivo_incoming_call_did = self.yaml_loader.load(
                        "config.yml", ["incomingCalls", yaml_incoming_call, "did"]
                    )
                    if int(xivo_incoming_call["did"]) == int(xivo_incoming_call_did):
                        try:
                            self.xivo_client.delete_incomingCall(
                                int(xivo_incoming_call["id"])
                            )
                        except Exception as e:
                            logger.error(
                                f"Error while deleting incoming call {yaml_incoming_call}: {e}"
                            )
                            raise e

        logger.info("Finished deleting the incoming calls")

    def delete_trunks(self):
        try:
            yaml_trunks = self.yaml_loader.load("config.yml", ["trunks"])
        except KeyError:
            logger.warning("No trunks found in config.yml, 0 trunk will be deleted")
            return

        xivo_trunks = self.xivo_client.list_trunks()

        logger.info("Start deleting the trunks part of the config.yml...")

        if xivo_trunks:
            for yaml_trunk in yaml_trunks:
                for xivo_trunk in xivo_trunks:
                    xivo_trunk_name = self.yaml_loader.load(
                        "config.yml", ["trunks", yaml_trunk, "name"]
                    )
                    if xivo_trunk["name"] == xivo_trunk_name:
                        try:
                            self.xivo_client.delete_trunk(int(xivo_trunk["id"]))
                            logger.debug(
                                f"deleted trunk {xivo_trunk['id']}:{xivo_trunk_name}"
                            )
                        except Exception as e:
                            logger.error(
                                f"Error while deleting trunk {yaml_trunk}: {e}"
                            )
                            raise e
                        break

        logger.info("Finished deleting the trunks part of the config.yml")

    def delete_groups(self):
        pass

    def delete_contexts(self):
        try:
            yaml_contexts = self.yaml_loader.load("config.yml", ["contexts"])
        except KeyError:
            logger.warning("No contexts found in config.yml, 0 context will be deleted")
            return

        logger.info("Start deleting the contexts part of the config.yml...")
        for yaml_context in yaml_contexts:
            try:
                logger.debug(f"Deleting context {yaml_context}")
                self.xivo_client.delete_context(yaml_context)

            except Exception as e:
                logger.error(f"Error while deleting context {yaml_context}: {e}")
                raise e

        logger.info("Finished deleting the contexts part of the config.yml")

    def add_contexts(self):
        try:
            yaml_contexts = self.yaml_loader.load("config.yml", ["contexts"])
        except KeyError:
            logger.warning("No contexts found in config.yml, 0 context will be created")
            return

        logger.info("Start creating the contexts part of the config.yml...")
        logger.debug(f"{yaml_contexts=}")
        for yaml_context in yaml_contexts:
            try:
                self.xivo_client.create_context(yaml_contexts[yaml_context])
            except Exception as e:
                if "already exists" in str(e) or "existe déjà" in str(e):
                    logger.warning(
                        f"Context {yaml_context} already exists, not checking if value are correct"
                    )
                else:
                    logger.error(f"Error while creating context {yaml_context}: {e}")
                    raise e

        logger.info("Finished creating the contexts part of the config.yml")

    def add_trunks(self):
        try:
            yaml_trunks = self.yaml_loader.load("config.yml", ["trunks"])
        except KeyError:
            logger.warning("No trunks found in config.yml, 0 trunk will be created")
            return

        logger.info("Start creating the trunks part of the config.yml...")

        for yaml_trunk in yaml_trunks:
            logger.debug(f"{yaml_trunk=}")
            try:
                self.xivo_client.create_trunk(yaml_trunks[yaml_trunk])
            except Exception as e:
                if "already exists" in str(e) or "existe déjà" in str(e):
                    logger.warning(
                        f"Trunk {yaml_trunk} already exists, not checking if value are correct"
                    )
                else:
                    logger.error(f"Error while creating trunk {yaml_trunk}: {e}")
                    raise e

        logger.info("Finished creating the trunks part of the config.yml")

    def add_incoming_calls(self):
        try:
            yaml_incoming_calls = self.yaml_loader.load("config.yml", ["incomingCalls"])
        except KeyError:
            logger.warning(
                "No incomingCalls found in config.yml, 0 incomingCalls will be created"
            )
            return

        logger.info("Start creating the incoming calls part of the config.yml...")

        xivo_users: List[User] = self.xivo_client.list_users()
        xivo_queues = self.xivo_client.list_queues()
        for yaml_incoming_call in yaml_incoming_calls:
            logger.debug(f"{yaml_incoming_call=}")
            try:
                self.xivo_client.create_incoming_call(
                    yaml_incoming_calls[yaml_incoming_call], xivo_users, xivo_queues
                )
            except Exception as e:
                if "already exists" in str(e) or "existe déjà" in str(e):
                    logger.warning(
                        f"Incoming call {yaml_incoming_call} already exists, not checking if value are correct"
                    )
                else:
                    logger.error(
                        f"Error while creating incoming call {yaml_incoming_call}: {e}"
                    )
                    raise e

        logger.info("Finished creating the incoming calls part of the config.yml")

    def add_queues(self):
        try:
            yaml_queues = self.yaml_loader.load("config.yml", ["queues"])
        except KeyError:
            logger.warning("No queues found in config.yml, 0 queue will be created")
            return

        logger.info("Start creating the queues part of the config.yml...")

        for yaml_queue in yaml_queues:
            logger.debug(f"{yaml_queue=}")
            try:
                self.xivo_client.create_queue(yaml_queues[yaml_queue])
            except Exception as e:
                if "already exists" in str(e) or "existe déjà" in str(e):
                    logger.warning(
                        f"Queue {yaml_queues[yaml_queue]['name']} already exists, not checking if values are correct"
                    )
                elif (
                    "Number outside of context's interval"
                    or "Le numero est hors interval du contexte" in str(e)
                ):
                    yaml_queue_number = self.yaml_loader.load(
                        "config.yml", ["queues", yaml_queue, "number"]
                    )
                    yaml_context = self.yaml_loader.load(
                        "config.yml", ["queues", yaml_queue, "agents", "context"]
                    )
                    logger.error(
                        f"Error while creating queue {yaml_queue}: {e}. You should check if the context '{yaml_context}' configuration and your queue number '{yaml_queue_number}' match"
                    )
                    raise e
                else:
                    logger.error(f"Error while creating queue {yaml_queue}: {e}")
                    raise e

            xivo_queues = self.xivo_client.list_queues()
            queue_id = None
            for xivo_queue in xivo_queues:
                if (xivo_queue["name"].lower() == yaml_queues[yaml_queue]["name"].lower()
                    or xivo_queue["displayName"].lower() == yaml_queues[yaml_queue]["display_name"].lower()):
                    queue_id = xivo_queue["id"]
                    break

            if queue_id is None:
                logger.error(f"No queue found with name '{yaml_queues[yaml_queue]['name']}'")
                continue  # Skip to the next queue

            if "agents" in yaml_queues[yaml_queue]:
                users = self._add_users("config.yml", ["queues", f"{yaml_queue}", "agents"])

                create_agents(
                    self.xivo_client,
                    queue_id,
                    user_prefix=self.yaml_loader.load(
                        "config.yml", ["queues", f"{yaml_queue}", "agents", "user_prefix"]
                    ),
                    first_exten=self.yaml_loader.load(
                        "config.yml", ["queues", f"{yaml_queue}", "agents", "first_number"]
                    ),
                    last_exten=self.yaml_loader.load(
                        "config.yml", ["queues", f"{yaml_queue}", "agents", "last_number"]
                    ),
                    context=self.yaml_loader.load(
                        "config.yml", ["queues", f"{yaml_queue}", "agents", "context"]
                    ),
                )

        logger.info("Finished creating the queues part of the config.yml")

    def _add_users(self, file_path: str, base_path: List[str] = []):
        logger.debug(f"Creating users...")

        # Creating label if not already exists
        try:
            yaml_user_label = self.yaml_loader.load(file_path, base_path + ["label"])
        except KeyError:
            logger.debug(
                f"No label found in {base_path}, user will be created without a label"
            )
            return

        xivo_labels = self.xivo_client.list_labels()
        if yaml_user_label not in [
            xivo_label.display_name for xivo_label in xivo_labels
        ]:
            logger.info(f"label {yaml_user_label} not found on XiVO, creating...")
            self.xivo_client.create_label(
                {
                    "display_name": yaml_user_label,
                    "description": f"label created by Populator for {base_path}",
                }
            )

        # Generate users
        users = create_users(
            user_prefix=self.yaml_loader.load(
                "config.yml", base_path + ["user_prefix"]
            ),
            line_type=self.yaml_loader.load("config.yml", base_path + ["line_type"]),
            first_exten=self.yaml_loader.load(
                "config.yml", base_path + ["first_number"]
            ),
            password="Userpassword123",
            last_exten=self.yaml_loader.load("config.yml", base_path + ["last_number"]),
            context=self.yaml_loader.load("config.yml", base_path + ["context"]),
            mds=self.yaml_loader.load("config.yml", base_path + ["mds"]),
            label_name=self.yaml_loader.load("config.yml", base_path + ["label"]),
        )

        # Create users in XiVO
        response = self.xivo_client.import_users_csv(users)

        yaml_line_type = self.yaml_loader.load("config.yml", base_path + ["line_type"])
        if yaml_line_type == "sip":

            def extract_line_ids_from_csv_import_response(
                user_api_response,
            ) -> List[str]:
                line_ids: List[str] = []
                response_json = user_api_response.json()
                line_ids = [user["line_id"] for user in response_json["created"]]
                return line_ids

            if self.dry_run:
                logger.info("DRY RUN: Would extract_line_ids_from_csv_import_response")
                return

            line_ids = extract_line_ids_from_csv_import_response(response)

            logger.debug(f"{response.text=}")

            devices: List[Any] = create_devices(
                first_exten=self.yaml_loader.load(
                    "config.yml", base_path + ["first_number"]
                ),
                amount=(
                    int(
                        self.yaml_loader.load("config.yml", base_path + ["last_number"])
                    )
                    - int(
                        self.yaml_loader.load(
                            "config.yml", base_path + ["first_number"]
                        )
                    )
                ),
            )

            device_ids: List[str] = []
            for device in devices:
                try:
                    response = self.xivo_client.create_device(device)
                except Exception as e:
                    logger.exception(
                        f"Error while creating device {device}, probably because it already exists OR device plugins in XiVO are not installed"
                    )
                    raise e

                device_ids.append(str(response.id))

            for line_id, device_id in zip(line_ids, device_ids):
                self.xivo_client.associate_line_device(
                    line_id=line_id, device_id=device_id
                )
        return users

    def add_simple_users(self):
        try:
            yaml_users = self.yaml_loader.load("config.yml", ["users"])
        except KeyError:
            logger.warning("No users found in config.yml, 0 user will be created")
            return

        logger.info("Start creating the users part of the config.yml...")

        for yaml_user in yaml_users:
            self._add_users("config.yml", ["users", f"{yaml_user}"])

        logger.info("Finished creating the users part of the config.yml")

    def delete_everything(self):
        """Delete ALL objects in XiVO!"""

        logger.info("Deleting all users...")
        self._delete_user(delete_all=True)

        logger.info("Deleting all agents...")
        xivo_agents = self.xivo_client.list_agents()
        if xivo_agents:
            _ = [self.xivo_client.delete_agent(agent.id) for agent in xivo_agents]

        logger.info("Deleting all queues...")
        xivo_queues = self.xivo_client.list_queues()
        if xivo_queues:
            _ = [
                self.xivo_client.delete_queue(int(queue["id"])) for queue in xivo_queues
            ]

        logger.info("Deleting all incoming calls...")
        xivo_incoming_calls = self.xivo_client.list_incoming_calls()
        if xivo_incoming_calls:
            _ = [self.xivo_client.delete_incomingCall(int(incoming_call["id"])) for incoming_call in xivo_incoming_calls]

        logger.info("Deleting all trunks...")
        xivo_trunks = self.xivo_client.list_trunks()
        if xivo_trunks:
            _ = [
                self.xivo_client.delete_trunk(int(trunk["id"])) for trunk in xivo_trunks
            ]

        logger.info("Deleting all contexts...")
        xivo_contexts = self.xivo_client.list_contexts()
        if xivo_contexts:
            _ = [
                self.xivo_client.delete_context(context["id"])
                for context in xivo_contexts
                if context["name"] != "__switchboard_directory"
            ]

        logger.info("Deleteing all labels...")
        xivo_labels = self.xivo_client.list_labels()
        if xivo_labels:
            _ = [self.xivo_client.delete_label(label.id) for label in xivo_labels]

        logger.info("Finished deleting all objects in XiVO")
