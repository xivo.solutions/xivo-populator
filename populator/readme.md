# XiVO Populator

## Configuring the XiVO Populator

To configure the XiVO Populator, you'll need to create `populator/code/config/config.yml` based on the template `populator/code/config/config.yml.template` . This template serves as the blueprint for your configuration, allowing you to define how the XiVO system should be set up or modified.
Dont forget to change the value base on your XiVO installation and how you want to populate your XiVO.


#### Configuration Guide for `config.yml`

The `populator/code/config.yml` file is a template that defines the settings for the XiVO Populator. It's divided into several sections, each responsible for a different aspect of the configuration.

#### Status

```yml
# Choose the operation mode (MANDATORY)
# 'Create' to set up new object, 'Delete' to remove existing object
# WARNING, mode 'DeleteEveything' will delete all XiVO object even if their not specify in this yml !

status: Create 
```

#### Special Mode

```yml
# Special mode for testing purposes (MANDATORY)
# Use 'dry' for a dry-run that will not making actual changes in XiVO but print it instead
specials: abcd 
```

#### Credentials

```yml
# Provide credentials to access your XiVO system (MANDATORY)
credentials:
  xivo_ip: "ajm1-xivo.ajmwisper.fr" # Replace with the IP address or FQDN of your XiVO PBX
  ws_user: api-all                  # Username of the API (webservice) access
  ws_pass: superpass1234            # Password of the API (webservice) access
  webi_login: root                  # Username of the web interface (webi) access
  webi_pwd: superpass               # Password of the web interface (webi) access
```

Anything after this is optional, provide at least on group of object, otherwise it will create nothing

#### Contexts

```yml
# Define contexts for call routing and handling
contexts:
  myfirstcontext:
    name: myfirstcontext             # Context name
    display_name: 'My First Context' # Display name for the context
    context_type: 'internal'         # Context type (e.g., Incall, Internal, etc.)
    # Specify sub-contexts if any
    include_sub_contexts:            
      - myfirstsubcontext
    # Define number ranges for different entity types within this context
    user:
      first_number: '10000'
      last_number:  '29999'
    # Similar blocks for 'group', 'queue', 'meetme', 'incall' 
    # ...

  # Define additional contexts like 'myfirstsubcontext' as needed
  # ...

```

#### Users

```yml
# Configuration for user creation
users:
  context: myfirstcontext           # Context where users will be created
  user_prefix: simpleUser           # Prefix for usernames
  line_type: sip                    # Type of line (sip or webrtc)
  first_number: '10000'             # Start of the number range for users
  last_number: '10009'              # End of the number range for users
  mds: default                      # Multi-Device Service (mds) setting
  label: 'Populator'                # Label for the users
```

All you user will have the name as prefix_extention (simpleUser_10000), 

If you want fake name instead of the prefix + extension, you can write *GenerateFakeName* instead of a prefix: 
```yml
  user_prefix: GenerateFakeName
```

This will result of fake name such as "April Johnson", "Christopher Adams", "Emily Greer", etc ...

#### Groups (Future Implementation)

```yml
# Configuration for group creation (not yet implemented)
groups:
  myfirstgroup:
    name: myfirstgroup
    number: '30001'
    # User configuration for the group
    users:
      context: default
      user_prefix: simpleUser
      line_type: sip
      # Number range for users in the group
      first_number: '10110'
      last_number:  '10119'
      mds: default
      label: 'Populator'
  # Define additional groups as needed
  # ...
```

#### Queues

```yml
# Setup for queues and their agents
queues: 
  myfirstqueue:
    name: myfirstqueue            # Queue name
    number: '40001'               # Queue number
    context: myfirstcontext       # Context for the queue
    agents:
      context: myfirstcontext     # Context for agents in this queue
      user_prefix: userQueueOne   # Prefix for agent usernames
      line_type: sip              # Line type for agents
      # Number range for agents
      first_number: '11100'
      last_number: '11110'
      mds: default                # MDS setting for agents
      label: 'Populator'          # Label for agents
  # Setup additional queues as required
  # ...

```

#### Trunk

```yml
trunks:
  my-first-trunk:
    name: myfirsttrunk
    authUsername: 'myfirsttrunk-username'
    password: myfirstpassword
    callerId: myfirstcallerId
    callLimit: "5"                    
    # Call limit for the trunk, Possible values : From 0 to x (0 being unlimited)
    connectionType: peer              
    # The connection type (user, peer or friend)
    addressingType: static            
    # The addressing type (static or dynamic) (If dynamic, no need to fill ipAddress field)
    ipAddress: "10.10.10.10"          
    # The Ip address if AddressingType is static
    context: "myfirstcontext"         
    # Context in which the trunk is created
    mediaServerId: "1"                
    # ID of the concerned mds (Value 1 for the MDS-main)
    nat: "no"                         
    # The NAT configuration (null, no, force_rport, comedia or force_rport or comedia)
  internaltests:
    name: "internaltests"
    authentification_username: "internaltests"
    password: "internaltests"
    caller_id: null
    call_limit: "0" # 0 for unlimited
    connection_type: "user"
    ip_addressing_type: "dynamic"
    context: "loadtest"
    mds_id: "1"
    nat: ""
```

## Usage

*After writing the config.yml*

### Start xivo-populator, that populate your XiVO based on the `config.yml`

Be carefull, starting the container, launch the scrip and will populate your XiVO

`docker compose -f docker-compose-populator.yml up --build`

Perfect ! all your XiVO is now populate with the objects you describe in config.yml