# XiVO Populator

XiVO Populator repo is a comprehensive solution for automating the setup, configuration, and testing of XiVO systems. 

It consists of three main components: 
- Populator for creating and managing XiVO entities
- Load tester for simulating SIP calls
- Gatling for simulating agent activities in queues

A quick guide for the demo environment that use all the funcionnality of this repo is available [here](README.demo.md)

## Table of Contents

- [XiVO Populator](#xivo-populator)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
    - [Populator](#populator)
    - [Load Tester](#load-tester)
    - [Gatling](#gatling)
  - [Prerequisites](#prerequisites)
  - [Getting Started](#getting-started)
  - [Configuration](#configuration)
    - [Populator Configuration](#populator-configuration)
    - [Load-tester Configuration](#load-tester-configuration)
    - [Gatling Configuration](#gatling-configuration)
  - [Usage](#usage)
    - [Populating XiVO](#populating-xivo)
    - [Load Testing](#load-testing)
      - [Tip: Create a Bash Function](#tip-create-a-bash-function)
    - [Gatling](#gatling-1)
  - [Contributing](#contributing)

## Features

### Populator
- Create and manage XiVO objects like contexts, users, queues, trunks, and incoming calls
- Bulk creation of entities using CSV imports
- Flexible configuration through YAML files
- Options for deleting specific entities or performing complete system cleanup

### Load Tester
- Simulate multiple XiVO users with configurable behaviors
- Run various scenarios like internal calls, queue calls, and agent status changes
- Customizable test parameters including number of users, spawn rate, and test duration

### Gatling
- Simulate activities of agents, pausing and unpausing agents based on weighted random selection

## Prerequisites

- Docker and Docker Compose
- Access to a XiVO system with API credentials

## Getting Started

1. Clone the repository with submodules:
   ```sh
   git clone --recurse-submodules git@gitlab.com:xivo.solutions/xivo-populator.git
   ```
2. Navigate to the project directory:
  ```cd xivo-populator```
1. Set up the configuration:

## Configuration

### Populator Configuration

The `config.yml` file in the `populator/code/config/` directory controls the behavior of the populator. Key configuration sections include:

- `status`: Set to 'Create', 'Delete', or 'DeleteEverything'
- `credentials`: XiVO system access details
- `contexts`, `users`, `queues`, `trunks`, `incomingCalls`: Entity configurations

For more information about this `populator/code/config/config.yml`, you can read the doc [here](populator/readme.md)

### Load-tester Configuration

Load tester is handle by a [conf.py](xivo-load-tester/etc/conf.py.sample) file, depend on the conf and scenarios, you need to provide a XiVO user export to `xivo-load-tester/etc/demo/user_export.csv`

More information about load-tester [here](xivo-load-tester/README.md)


### Gatling Configuration

The configuration for the Gatling is stored in a YAML file in [config/config.yml](gatling/code/config/config.yml), an example is available [here](gatling/code/config/config.yml.example)

Also you need to provide a XiVO user export to `gatling/code/config/user_export.csv`

More information about gatling [here](gatling/README.md)


## Usage

### Populating XiVO

1. Setup [Populator Configuration](#populator-configuration)
2. Run the populator:
`docker-compose -f docker-compose-populator.yml up --build`

The populator will create, delete, or modify entities based on the `status` setting in `config.yml`.

### Load Testing

1. Setup [Load Testing configuration](#load-tester-configuration)
2. Start the load testing container:
`docker-compose -f docker-compose-load.yml up -d --build`
3. Run a specific scenario:
`docker exec -it xivo-load sh -c "./load-tester -c etc/load/conf-load-sogetrel.py scenarios/sogetrel/call-internal"`
or
`docker exec -it xivo-load sh -c "./load-tester -c etc/load/conf-load-sogetrel.py scenarios/sogetrel/register-internal/"`

#### Tip: Create a Bash Function

Add this function to your `~/.bashrc` for easier scenario execution:

```bash
function xivo_load {
docker exec -it xivo-load sh -c "./load-tester -c etc/load/conf-load-sogetrel.py scenarios/$1/"
}
```
Then you can run scenarios like this:
`xivo_load sogetrel/register-internal`
### Gatling

1. Setup [Gatling configuration](#gatling-configuration)
2. Start the gatling:
`docker compose -f docker-compose-gatling.yml up -d --build`

## Contributing
Contributions to XiVO Populator are welcome. Please ensure you follow the existing code style and include tests for new features. For major changes, please open an issue first to discuss what you would like to change.
