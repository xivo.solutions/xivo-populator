# Xivo gatling

This Python script simulates the behavior of Xivo agent by logging them in, updating their status (Pause and Unpause) with handling WebSocket messages. The script reads user credentials from a CSV file and applies optional filters based on labels and number ranges.

## Prerequisites

Docker and Docker Compose installed on your system.

## Configuration

The configuration for the script is stored in a YAML file (config/config.yml). Here's a breakdown of the configuration options:

- `xivocc_ip`: The IP address of the Xivo Call Center server.
- `xuc_port`: The port number for the Xivo User Client (XUC) API.
- `run_time`: The duration for which the script should run. Set to 0 for infinite runtime.
- `users_file`:
  - `csv_path`: The path to the CSV file containing user credentials.
  - `filter`:
    - `labels`: A list of labels to filter users based on their labels in the CSV file.
    - `number_range`: A range of numbers to filter users based on their extension numbers in the CSV file.

Also, a CSV file need to be provide.

Dont forget to add your XiVO users export to `code/config/user_export.csv`, the csv need to have a least `username`, `password` and `exten`

## Usage

1. Place your XiVO users export CSV file in the config directory and rename it to user_export.csv. The CSV file should have at least username, password, and exten columns.

1. Configure the script by editing the config/config.yml file.

1. Build and start the Docker container using Docker Compose:
    ```bash
    docker compose up -d
    ```

1. To stop the script, press Ctrl+C or send a SIGINT or SIGTERM signal.