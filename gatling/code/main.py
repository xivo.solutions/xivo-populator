import os
import time
import json
import random
import csv
import threading
import queue
import websocket
import ssl
import requests
import atexit
import logging
import yaml
from concurrent.futures import ThreadPoolExecutor
from urllib3.exceptions import InsecureRequestWarning

# Disable SSL warnings
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Define constants
AGENT_READY = "AgentReady"
AGENT_LOGGED_OUT = "AgentLoggedOut"
AGENT_LOGIN = "agentLogin"
AGENT_LOGOUT = "agentLogout"
AGENT_PAUSE = "pauseAgent"
AGENT_UNPAUSE = "unpauseAgent"
AGENT_STATES_RESPONSE = "AgentStatesResponse"
AGENT_STATE_EVENT = "AgentStateEvent"
GET_AGENT_STATES = "getAgentStates"
WEB_CLASS = "web"
CONFIG_FILE = 'config/config.yml'
USER_DATA_FILE = 'config/user_export.csv'

# WebSocket connection retry parameters
WS_RECONNECT_INTERVAL = 5
WS_MAX_RETRIES = 3

class XivoUser:
    """Represents a Xivo user and manages communication with the Xivo server."""

    def __init__(self, username, password, number, xivocc_ip, xuc_port):
        self.username = username
        self.password = password
        self.number = number
        self.xivocc_ip = xivocc_ip
        self.xuc_port = xuc_port
        self.token = None
        self.ws = None
        self.current_status = None
        self.running = True
        self.agent_id = None
        self.agent_states = None
        self.message_queue = queue.Queue()
        self.lock = threading.Lock()

    def _get_token(self):
        """Fetches an authentication token from the Xivo server."""
        url = f"http://{self.xivocc_ip}:{self.xuc_port}/xuc/api/2.0/auth/login"
        try:
            response = requests.post(url, json={"login": self.username, "password": self.password}, verify=False)
            response.raise_for_status()
            return response.json()["token"]
        except requests.RequestException as e:
            logging.error(f"Error getting token for user {self.username}: {e}")
            return None

    def _create_websocket_connection(self):
        """Establishes a WebSocket connection to the Xivo server."""
        ws_url = f"ws://{self.xivocc_ip}:{self.xuc_port}/xuc/api/2.0/cti?token={self.token}"
        try:
            return websocket.create_connection(ws_url, sslopt={"cert_reqs": ssl.CERT_NONE})
        except websocket.WebSocketException as e:
            logging.error(f"Error creating WebSocket connection for user {self.username}: {e}")
            return None

    def _run_websocket_listener(self):
        """Continuously receives and processes messages from the WebSocket connection."""
        while self.running:
            try:
                message = self.ws.recv()
                self._process_message(message)
            except websocket.WebSocketException as e:
                logging.error(f"WebSocket error for user {self.username}: {e}. Reconnecting...")
                if not self._reconnect_websocket():
                    break  # Exit the loop if reconnection fails

    def _process_message(self, message):
        """Parses and handles incoming WebSocket messages."""
        try:
            data = json.loads(message)
            msg_type = data.get("msgType")
            if msg_type == AGENT_STATES_RESPONSE:
                self._process_agent_states_response(data)
            elif msg_type == AGENT_STATE_EVENT:
                self._process_agent_state_event(data)
            else:
                logging.debug(f"Received unknown message type: {msg_type}")
        except json.JSONDecodeError:
            logging.error(f"Failed to parse message for user {self.username}: {message}")

    def _process_agent_states_response(self, data):
        """Extracts and updates agent state information."""
        all_agent_states = data.get("ctiMessage", {}).get("agentStates", [])
        self.agent_states = next((state for state in all_agent_states if state.get("phoneNb") == self.number), None)
        logging.info(f"Agent states for user {self.username}: {self.agent_states}")

    def _process_agent_state_event(self, data):
        """Handles AgentStateEvent messages."""
        event = data.get("ctiMessage", {})
        if event.get("phoneNb") == self.number:
            if event.get("name") == AGENT_LOGGED_OUT:
                self._handle_agent_logout(event)
            elif event.get("name") == AGENT_READY:
                self.agent_states = AGENT_READY
            logging.info(f"Received agent state event for user {self.username}: {data}")

    def _handle_agent_logout(self, event):
        """Handles agent logout events and attempts re-login."""
        self.agent_id = event.get("agentId")
        logging.info(f"Agent {self.username} (ID: {self.agent_id}) logged out. Attempting to log in.")
        self.agent_login()

    def _send_ws_message(self, message):
        """Sends a message through the WebSocket connection."""
        with self.lock:
            if self.ws:
                try:
                    self.ws.send(json.dumps(message))
                except websocket.WebSocketException as e:
                    logging.error(f"WebSocket error for user {self.username}: {e}. Reconnecting...")
                    self._reconnect_websocket()
            else:
                logging.warning(f"WebSocket connection not established for user {self.username}.")

    def _reconnect_websocket(self):
        """Attempts to reconnect the WebSocket connection with retries."""
        self.ws = None
        for attempt in range(WS_MAX_RETRIES):
            logging.info(f"Reconnecting WebSocket for user {self.username} (attempt {attempt+1}/{WS_MAX_RETRIES})...")
            self.token = self._get_token()
            if self.token:
                self.ws = self._create_websocket_connection()
                if self.ws:
                    logging.info(f"WebSocket reconnected for user {self.username}.")
                    self.agent_login()
                    return True
            time.sleep(WS_RECONNECT_INTERVAL)
        logging.error(f"Failed to reconnect WebSocket for user {self.username} after {WS_MAX_RETRIES} attempts.")
        return False

    def agent_login(self):
        """Sends an agent login request."""
        login_message = {"claz": WEB_CLASS, "command": AGENT_LOGIN, "agentphonenumber": self.number}
        self._send_ws_message(login_message)
        logging.info(f"Sent login request for user {self.username}")

    def agent_logout(self):
        """Sends an agent logout request."""
        logout_message = {"claz": WEB_CLASS, "command": AGENT_LOGOUT}
        self._send_ws_message(logout_message)
        logging.info(f"Sent logout request for user {self.username}")

    def send_get_agent_states(self):
        """Requests agent state information."""
        self._send_ws_message({"claz": WEB_CLASS, "command": GET_AGENT_STATES})

    def update_status(self):
        """Randomly changes the agent's status."""
        statuses = [AGENT_PAUSE, AGENT_UNPAUSE]
        weights = [0.3, 0.7]
        next_status = random.choices(statuses, weights=weights, k=1)[0]
        if next_status != self.current_status:
            self.current_status = next_status
            self._send_ws_message({"claz": WEB_CLASS, "command": next_status})
            logging.info(f"User {self.username} changed status to: {next_status}")

    def start_user(self):
        """Starts the user's activity."""
        time.sleep(random.uniform(0, 5))
        self.token = self._get_token()
        if self.token:
            self.ws = self._create_websocket_connection()
            time.sleep(2)  # Give some time for the WebSocket connection to be established and Xuc to be ready
            if self.ws:
                self.agent_login()
                listener_thread = threading.Thread(target=self._run_websocket_listener)
                listener_thread.daemon = True
                listener_thread.start()

                while self.running:
                    if random.random() < 0.2:
                        self.update_status()

                    self.send_get_agent_states()
                    time.sleep(2)
            else:
                logging.error(f"Failed to establish initial WebSocket connection for user {self.username}.")
        else:
            logging.error(f"Failed to obtain token for user {self.username}.")

    def stop_user(self):
        """Gracefully stops the user's activity."""
        self.running = False
        if self.ws:
            self.agent_logout()
            time.sleep(1)  # Allow time for logout message to be sent
            self.ws.close()

def read_user_credentials(csv_file_path, filter_params=None):
    """Reads user credentials from a CSV file with optional filtering."""
    users_credentials = []
    with open(csv_file_path) as export_csv_from_xivo:
        export_csv = csv.DictReader(export_csv_from_xivo)
        for row in export_csv:
            if filter_params:
                labels = filter_params.get('labels')
                number_range = filter_params.get('number_range')

                if labels and "labels" in row:
                    row_labels = row["labels"].split(",")
                    if not any(label in row_labels for label in labels):
                        continue

                if number_range:
                    start, end = number_range
                    if not (str(start) <= row.get("exten", "") <= str(end)):
                        continue

            user_information = {
                "username": row["username"],
                "password": row["password"],
                "number": row["exten"],
            }
            users_credentials.append(user_information)

    return users_credentials

def start_user_thread(user):
    """Starts a separate thread for each user."""
    try:
        user.start_user()
    except Exception as e:
        logging.error(f"Error in user thread for {user.username}: {e}")
    finally:
        user.stop_user()

def cleanup(xivo_users, executor):
    """Performs cleanup and graceful shutdown."""
    logging.info("Initiating cleanup...")
    for user in xivo_users:
        user.stop_user()
    logging.info("Waiting for all user threads to finish...")
    executor.shutdown(wait=True)
    logging.info("Cleanup completed.")


def main():
    """Main function to orchestrate the process."""
    try:
        with open(CONFIG_FILE) as f:
            config = yaml.safe_load(f)

        xivocc_ip = config.get('xivocc_ip', 'localhost')
        xuc_port = config.get('xuc_port', 8090)
        run_time = config.get('run_time', 0)

        users_file = config.get('users_file', {})
        csv_file_path = users_file.get('csv_path', USER_DATA_FILE)
        filter_params = users_file.get('filter', {})

        user_credentials = read_user_credentials(csv_file_path, filter_params)

        number_users = len(user_credentials)
        xivo_users = [XivoUser(cred["username"], cred["password"], cred["number"], xivocc_ip, xuc_port)
                      for cred in user_credentials]

        executor = ThreadPoolExecutor(max_workers=number_users)
        atexit.register(cleanup, xivo_users, executor)

        user_futures = [executor.submit(start_user_thread, user) for user in xivo_users]

        try:
            if run_time != 0:
                time.sleep(run_time)
            else:
                while True:
                    time.sleep(60)
        except (KeyboardInterrupt, SystemExit):
            logging.info("Received exit signal. Initiating graceful shutdown...")
        finally:
            logging.info("Finished running. Initiating graceful shutdown...")

    except yaml.YAMLError as e:
        logging.error(f"Error parsing YAML file: {e}")
    except KeyError as e:
        logging.error(f"Missing required field in YAML file: {e}")
    except Exception as e:
        logging.error(f"An unexpected error occurred: {e}")

if __name__ == "__main__":
    main()